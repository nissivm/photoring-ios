//
//  AFI_ClasseAuxiliar.h
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 25/12/14.
//  Copyright (c) 2014 Nissi Vieira Miranda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFI_ClasseAuxiliar : NSObject

@property (assign, nonatomic) double maxStageWidth;
@property (assign, nonatomic) double maxStageHeight;

+ (AFI_ClasseAuxiliar *)sharedAuxiliar;
- (UIImage *)getRedimensionedImage:(UIImage *)image;
- (UIImage *)createNonRetinaImage:(UIImage *)image;
- (UIImage *)getImageUnderProportion:(UIImage *)image;
- (UIImage *)maskImage:(UIImage *)image withMask:(UIImage *)maskImage;
- (NSString *)getImageOrientation:(UIImage *)image;

@end
