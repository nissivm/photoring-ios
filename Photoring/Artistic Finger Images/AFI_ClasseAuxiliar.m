//
//  AFI_ClasseAuxiliar.m
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 25/12/14.
//  Copyright (c) 2014 Nissi Vieira Miranda. All rights reserved.
//

#import "AFI_ClasseAuxiliar.h"

static AFI_ClasseAuxiliar *editor;

@implementation AFI_ClasseAuxiliar

+ (AFI_ClasseAuxiliar *)sharedAuxiliar
{
    if(!editor)
        editor = [[AFI_ClasseAuxiliar alloc] init];
    
    return editor;
}

- (UIImage *)getRedimensionedImage:(UIImage *)image
{
    double novaLargura;
    double novaAltura;
    
    if(image.size.width > image.size.height)
    {
        novaLargura = image.size.width > self.maxStageWidth ? self.maxStageWidth : image.size.width;
        novaAltura = (novaLargura * image.size.height)/image.size.width;
        
        if(novaAltura > self.maxStageHeight)
        {
            novaAltura = self.maxStageHeight;
            novaLargura = (novaAltura * image.size.width)/image.size.height;
        }
    }
    else if(image.size.width < image.size.height)
    {
        novaAltura = image.size.height > self.maxStageHeight ? self.maxStageHeight : image.size.height;
        novaLargura = (novaAltura * image.size.width)/image.size.height;
        
        if(novaLargura > self.maxStageWidth)
        {
            novaLargura = self.maxStageWidth;
            novaAltura = (novaLargura * image.size.height)/image.size.width;
        }
    }
    else
    {
        novaAltura = image.size.height > self.maxStageHeight ? self.maxStageHeight : image.size.height;
        novaLargura = novaAltura;
    }
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(novaLargura, novaAltura), YES, 0.0);
    [image drawInRect:CGRectMake(0, 0, novaLargura, novaAltura)];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (UIImage *)createNonRetinaImage:(UIImage *)image
{
    double novaLargura = image.size.width/2;
    double novaAltura = image.size.height/2;
    
    // Se colocar "YES", a foto fica preta
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(novaLargura, novaAltura), NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, novaLargura, novaAltura)];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (UIImage *)getImageUnderProportion:(UIImage *)image
{
    BOOL needsRedimension = FALSE;
    UIView *view;
    UIImageView *imageView;
    
    double novaLargura = 0.0;
    double novaAltura = 0.0;
    
    if(image.size.width > image.size.height) // landscape
    {
        float appProportion = 1.52;
        
        if(image.size.width/image.size.height > appProportion)
        {
            needsRedimension = TRUE;
            novaLargura = image.size.height * appProportion;
            novaAltura = image.size.height;
        }
        else if(image.size.width/image.size.height < appProportion)
        {
            needsRedimension = TRUE;
            novaLargura = image.size.width;
            novaAltura = novaLargura/appProportion;
        }
    }
    else if(image.size.width < image.size.height) // portrait
    {
        float appProportion = 1.35;
        
        if(image.size.height/image.size.width > appProportion)
        {
            needsRedimension = TRUE;
            novaLargura = image.size.width;
            novaAltura = image.size.width * appProportion;
        }
        else if(image.size.height/image.size.width < appProportion)
        {
            needsRedimension = TRUE;
            novaAltura = image.size.height;
            novaLargura = novaAltura/appProportion;
        }
    }
    
    if(needsRedimension)
    {
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, image.size.width, image.size.height)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.backgroundColor = [UIColor clearColor];
        imageView.image = image;
        
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, novaLargura, novaAltura)];
        view.backgroundColor = [UIColor clearColor];
        [view addSubview:imageView];
        
        UIGraphicsBeginImageContextWithOptions(view.frame.size, YES, 0.0);
        [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    return image;
}

- (UIImage *)maskImage:(UIImage *)image withMask:(UIImage *)maskImage
{
    CGImageRef maskRef = maskImage.CGImage;
    
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
    
    CGImageRef maskedImageRef = CGImageCreateWithMask([image CGImage], mask);
    UIImage *maskedImage = [UIImage imageWithCGImage:maskedImageRef];
    
    CGImageRelease(mask);
    CGImageRelease(maskedImageRef);
    
    // returns new image with mask applied
    return maskedImage;
}

- (NSString *)getImageOrientation:(UIImage *)image
{
    double imageWidth = image.size.width;
    double imageHeight = image.size.height;
    NSString *imageOrientation;
    
    if(imageWidth > imageHeight)
        imageOrientation = @"landscape";
    else if(imageWidth < imageHeight)
        imageOrientation = @"portrait";
    else
        imageOrientation = @"square";
    
    return imageOrientation;
}

@end
