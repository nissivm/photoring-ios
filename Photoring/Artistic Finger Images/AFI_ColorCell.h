//
//  AFI_ColorCell.h
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 29/03/15.
//  Copyright (c) 2015 Nissi Vieira Miranda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFI_ColorCell : UICollectionViewCell

@property (nonatomic, strong) NSString *colorCode;
@property (nonatomic, weak) IBOutlet UIView *colorSample;

@end
