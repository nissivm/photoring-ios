//
//  AFI_ColorsContainerView.m
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 29/03/15.
//  Copyright (c) 2015 Nissi Vieira Miranda. All rights reserved.
//

#import "AFI_ColorsContainerView.h"
#import "UIColor+HexToUIColor.h"
#import "AFI_ColorCell.h"

@interface AFI_ColorsContainerView () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *inUseColorView;

@end

@implementation AFI_ColorsContainerView
{
    NSMutableArray *hexColors;
    NSString *hexColorInUse;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        hexColors = [@[] mutableCopy];
        
        NSData *data = [NSData dataWithContentsOfFile: [[NSBundle mainBundle] pathForResource:@"colors" ofType:@"json"]];
        NSDictionary *colorsJson = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSArray *colors = [colorsJson objectForKey:@"colors"];
        
        for(NSDictionary *colorSpecs in colors)
        {
            [hexColors addObject:[colorSpecs objectForKey:@"colorCode"]];
        }
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImage *maskingImage = [UIImage imageNamed:@"paintMask.png"];
    UIImageView *maskImageView = [[UIImageView alloc] initWithImage:maskingImage];
    [maskImageView setFrame:[self.inUseColorView bounds]];
    [[self.inUseColorView layer] setMask:[maskImageView layer]];
    
    self.inUseColorView.backgroundColor = [UIColor whiteColor];
    
    hexColorInUse = [hexColors objectAtIndex:hexColors.count - 2];
}

//----------------------------------------------------------------------------------//
#pragma mark - IBActions
//----------------------------------------------------------------------------------//

- (IBAction)closeButtonTapped:(UIButton *)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveColorContView"
                                                        object:nil];
}

//----------------------------------------------------------------------------------//
#pragma mark - UICollectionViewDataSource
//----------------------------------------------------------------------------------//

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return [hexColors count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AFI_ColorCell *cell = (AFI_ColorCell *)[cv dequeueReusableCellWithReuseIdentifier:@"ColorCell"
                                                                       forIndexPath:indexPath];
    
    UIImage *maskingImage = [UIImage imageNamed:@"roundMask.png"];
    UIImageView *maskImageView = [[UIImageView alloc] initWithImage:maskingImage];
    [maskImageView setFrame:[cell.colorSample bounds]];
    [[cell.colorSample layer] setMask:[maskImageView layer]];
    
    cell.colorCode = [hexColors objectAtIndex:indexPath.item];
    cell.colorSample.backgroundColor = [UIColor colorWithHexString:cell.colorCode];
    
    return cell;
}

//----------------------------------------------------------------------------------//
#pragma mark - UICollectionViewDelegateFlowLayout
//----------------------------------------------------------------------------------//

- (CGSize)collectionView:(UICollectionView *)cv
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float cellSide = self.collectionView.frame.size.width/9;
    
    return CGSizeMake(cellSide, cellSide);
}

- (CGFloat)collectionView:(UICollectionView *)cv
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)cv
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

//-------------------------------------------------------------------------//
#pragma mark - UICollectionViewDelegate
//-------------------------------------------------------------------------//

- (void)collectionView:(UICollectionView *)cv didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    AFI_ColorCell *cell = (AFI_ColorCell *)[cv cellForItemAtIndexPath:indexPath];
    
    if([cell.colorCode isEqualToString:hexColorInUse] == NO)
    {
        self.inUseColorView.backgroundColor = cell.colorSample.backgroundColor;
        hexColorInUse = cell.colorCode;
        
        NSString *whiteColor = [hexColors objectAtIndex:hexColors.count - 2];
        if([cell.colorCode isEqualToString:whiteColor])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ColorChanged"
                                                                object:[UIColor clearColor]];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ColorChanged"
                                                                object:cell.colorSample.backgroundColor];
        }
    }
}

//-------------------------------------------------------------------------//
#pragma mark - Memory Warning
//-------------------------------------------------------------------------//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
