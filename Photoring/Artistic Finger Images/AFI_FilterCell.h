//
//  AFI_FilterCell.h
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 26/12/14.
//  Copyright (c) 2014 Nissi Vieira Miranda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFI_FilterCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *filterSample;
@property (nonatomic, weak) IBOutlet UIImageView *filterSelection;

@end
