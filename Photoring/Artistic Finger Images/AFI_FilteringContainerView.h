//
//  AFIFilteringViewController.h
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 29/12/14.
//  Copyright (c) 2014 Nissi Vieira Miranda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFI_FilteringContainerView : UIViewController

@end
