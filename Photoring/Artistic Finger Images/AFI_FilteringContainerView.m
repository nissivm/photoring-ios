//
//  AFI_FilteringViewController.m
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 29/12/14.
//  Copyright (c) 2014 Nissi Vieira Miranda. All rights reserved.
//

#import "AFI_FilteringContainerView.h"
#import "GPUImage.h"

@interface AFI_FilteringContainerView ()

@end

@implementation AFI_FilteringContainerView
{
    GPUImageOutput<GPUImageInput> *lookupFilter;
    
    GPUImagePicture *gpuImageToFilter;
    GPUImageView *imageView;
    NSString *lookupAddr;
    NSUserDefaults *userDefaults;
    
    BOOL hasFilterApplied;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    hasFilterApplied = FALSE;
    
    CGRect mainScreenFrame = [[UIScreen mainScreen] applicationFrame];
    GPUImageView *primaryView = [[GPUImageView alloc] initWithFrame:mainScreenFrame];
    self.view = primaryView;
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    UIImage *imagemOriginal = [UIImage imageWithData:[userDefaults objectForKey:@"originalImage"]];
    
    lookupFilter = [[GPUImageLookupFilter alloc] init];
    
    gpuImageToFilter = [[GPUImagePicture alloc] initWithImage:imagemOriginal];
    imageView = (GPUImageView *)self.view;
    imageView.backgroundColor = [UIColor clearColor];
    [imageView setBackgroundColorRed:0 green:0 blue:0 alpha:0];
    
    // Filtragem inicial:
    
    GPUImageOutput<GPUImageInput> *filter = [[GPUImageFilter alloc] init];
    [gpuImageToFilter addTarget:filter];
    [filter addTarget:imageView];
    [gpuImageToFilter processImage];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applyFilter:) name:@"ApplyFilter" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeFilters) name:@"RemoveFilters" object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//-------------------------------------------------------------------------//
#pragma mark - Notifications
//-------------------------------------------------------------------------//

- (void)applyFilter:(NSNotification *)notification
{
    hasFilterApplied = TRUE;
    [self applyFilterAtIndex:[notification.object intValue]];
}

- (void)removeFilters
{
    if(hasFilterApplied)
    {
        hasFilterApplied = FALSE;
        
        [gpuImageToFilter removeAllTargets];
        [gpuImageToFilter addTarget:imageView];
        [gpuImageToFilter processImage];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LookupFilter"
                                                            object:Nil];
    }
}

//-------------------------------------------------------------------------//
#pragma mark - Filters Switch
//-------------------------------------------------------------------------//

- (void)applyFilterAtIndex:(int)index
{
    index++;
    
    switch(index)
    {
        case 1:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"Accion" ofType:@"png"];
            break;
            
        case 2:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"Action16" ofType:@"png"];
            break;
            
        case 3:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"Aged" ofType:@"png"];
            break;
            
        case 4:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"Amatorka" ofType:@"png"];
            break;
            
        case 5:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"Autumn" ofType:@"png"];
            break;
            
        case 6:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"Bella" ofType:@"png"];
            break;
            
        case 7:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"BlueEvening" ofType:@"png"];
            break;
            
        case 8:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"Cinema" ofType:@"png"];
            break;
            
        case 9:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"ColdWhite" ofType:@"png"];
            break;
            
        case 10:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"CreamyNoon" ofType:@"png"];
            break;
            
        case 11:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"CrossProcessing" ofType:@"png"];
            break;
            
        case 12:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"CyanPastel" ofType:@"png"];
            break;
            
        case 13:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"FalseColorInfrared" ofType:@"png"];
            break;
            
        case 14:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"ForRed" ofType:@"png"];
            break;
            
        case 15:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"GreenishTones" ofType:@"png"];
            break;
            
        case 16:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"LordKelvin" ofType:@"png"];
            break;
            
        case 17:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"MagicalEffectOne" ofType:@"png"];
            break;
            
        case 18:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"MagicalEffectThree" ofType:@"png"];
            break;
            
        case 19:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"MagicalEffectTwo" ofType:@"png"];
            break;
            
        case 20:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"MatteBlackWhite" ofType:@"png"];
            break;
            
        case 21:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"MediumPurple" ofType:@"png"];
            break;
            
        case 22:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"MidsummerLight" ofType:@"png"];
            break;
            
        case 23:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"Nashville" ofType:@"png"];
            break;
            
        case 24:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"OldCamera" ofType:@"png"];
            break;
            
        case 25:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"Olivia" ofType:@"png"];
            break;
            
        case 26:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"PaleRed" ofType:@"png"];
            break;
            
        case 27:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"ParisianRain" ofType:@"png"];
            break;
            
        case 28:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"PeacefulAutumn" ofType:@"png"];
            break;
            
        case 29:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"Portrait" ofType:@"png"];
            break;
            
        case 30:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"Sepia05" ofType:@"png"];
            break;
            
        case 31:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"StandardInfrared" ofType:@"png"];
            break;
            
        case 32:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"StandardMono" ofType:@"png"];
            break;
            
        case 33:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"Steampunk" ofType:@"png"];
            break;
            
        case 34:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"SunnyDay" ofType:@"png"];
            break;
            
        case 35:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"VintageBrown" ofType:@"png"];
            break;
            
        case 36:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"VintageOne" ofType:@"png"];
            break;
            
        case 37:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"VintageTwo" ofType:@"png"];
            break;
            
        case 38:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"WarmSpringLight" ofType:@"png"];
            break;
            
        case 39:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"WashedAway" ofType:@"png"];
            break;
            
        case 40:
            lookupAddr = [[NSBundle mainBundle] pathForResource:@"YellowVintage" ofType:@"png"];
            break;
    }
    
    UIImage *lookupImage = [UIImage imageWithContentsOfFile:lookupAddr];
    GPUImagePicture *lookupGPUImage = [[GPUImagePicture alloc] initWithImage:lookupImage];
    
    [gpuImageToFilter addTarget:lookupFilter];
    [lookupGPUImage addTarget:lookupFilter];
    [lookupFilter addTarget:imageView];
    
    [gpuImageToFilter processImage];
    [lookupGPUImage processImage];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LookupFilter"
                                                        object:lookupFilter];
}

//-------------------------------------------------------------------------//
#pragma mark - Memory Warning
//-------------------------------------------------------------------------//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
