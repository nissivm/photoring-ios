//
//  AFIFiltersViewController.m
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 25/12/14.
//  Copyright (c) 2014 Nissi Vieira Miranda. All rights reserved.
//

#import "AFI_FiltersContainerView.h"
#import "AFI_FilterCell.h"

@interface AFI_FiltersContainerView () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation AFI_FiltersContainerView
{
    NSArray *samples;
    NSInteger selFilterIndex;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    samples = [NSArray arrayWithObjects:@"accion_sample", @"action16_sample", @"aged_sample", @"amatorka_sample", @"autumn_sample", @"bella_sample", @"blueEvening_sample",
                                        @"cinema_sample", @"coldWhite_sample", @"creamyNoon_sample", @"crossProcessing_sample", @"cyanPastel_sample", @"falseColorInfrared_sample",
                                        @"forRed_sample", @"greenishTones_sample", @"lordKelvin_sample", @"magicalEffectOne_sample", @"magicalEffectThree_sample",
                                        @"magicalEffectTwo_sample", @"matteBlackWhite_sample", @"mediumPurple_sample", @"midsummerLight_sample", @"nashville_sample",
                                        @"oldCamera_sample", @"olivia_sample", @"paleRed_sample", @"parisianRain_sample", @"peacefulAutumn_sample", @"portrait_sample",
                                        @"sepia05_sample", @"standardInfrared_sample", @"standardMono_sample", @"steampunk_sample", @"sunnyDay_sample",
                                        @"vintageBrown_sample", @"vintageOne_sample", @"vintageTwo_sample", @"warmSpringLight_sample", @"washedAway_sample",
                                        @"yellowVintage_sample", nil];
    
    selFilterIndex = -1;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(filtersVCExposed) name:@"FiltersVCExposed" object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//----------------------------------------------------------------------------------//
#pragma mark - Notifications
//----------------------------------------------------------------------------------//

- (void)filtersVCExposed
{
    if(selFilterIndex >= 0)
    {
        NSIndexPath *idxPath = [NSIndexPath indexPathForItem:selFilterIndex inSection:0];
        [self.collectionView scrollToItemAtIndexPath:idxPath
                                    atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                            animated:YES];
    }
    else
    {
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]
                                    atScrollPosition:UICollectionViewScrollPositionLeft
                                            animated:YES];
    }
}

//----------------------------------------------------------------------------------//
#pragma mark - IBActions
//----------------------------------------------------------------------------------//

- (IBAction)closeButtonTapped:(UIButton *)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseContainerView"
                                                        object:nil];
}

- (IBAction)originalImageButtonTapped:(UIButton *)sender
{
    NSIndexPath *idxPath = [NSIndexPath indexPathForItem:selFilterIndex inSection:0];
    AFI_FilterCell *selectedCell = (AFI_FilterCell *)[self.collectionView cellForItemAtIndexPath:idxPath];
    selectedCell.filterSelection.hidden = YES;
    
    selFilterIndex = -1;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveFilters"
                                                        object:nil];
}

//----------------------------------------------------------------------------------//
#pragma mark - UICollectionViewDataSource
//----------------------------------------------------------------------------------//

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)cv
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)cv numberOfItemsInSection:(NSInteger)section
{
    return [samples count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AFI_FilterCell *cell = (AFI_FilterCell *)[cv dequeueReusableCellWithReuseIdentifier:@"FilterCell"
                                                                         forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor clearColor];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:[samples objectAtIndex:indexPath.row] ofType:@"png"];
    UIImage *filterSample = [UIImage imageWithContentsOfFile:path];
    
    cell.filterSample.image = filterSample;
    
    if(indexPath.row == selFilterIndex)
        cell.filterSelection.hidden = NO;
    else
        cell.filterSelection.hidden = YES;
    
    return cell;
}

//----------------------------------------------------------------------------------//
#pragma mark - UICollectionViewDelegateFlowLayout
//----------------------------------------------------------------------------------//

- (CGSize)collectionView:(UICollectionView *)cv
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float cellSide = self.collectionView.frame.size.width/5;
    
    return CGSizeMake(cellSide, cellSide);
}

- (CGFloat)collectionView:(UICollectionView *)cv
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)cv
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

//-------------------------------------------------------------------------//
#pragma mark - UICollectionViewDelegate
//-------------------------------------------------------------------------//

- (void)collectionView:(UICollectionView *)cv didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    AFI_FilterCell *selectedCell;
    
    if(selFilterIndex >= 0)
    {
        NSIndexPath *idxPath = [NSIndexPath indexPathForItem:selFilterIndex inSection:0];
        selectedCell = (AFI_FilterCell *)[cv cellForItemAtIndexPath:idxPath];
        selectedCell.filterSelection.hidden = YES;
    }
    
    selectedCell = (AFI_FilterCell *)[cv cellForItemAtIndexPath:indexPath];
    selectedCell.filterSelection.hidden = NO;
    
    selFilterIndex = indexPath.row;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ApplyFilter"
                                                        object:[NSNumber numberWithInteger:selFilterIndex]];
}

//----------------------------------------------------------------------------------//
#pragma mark - Memory Warning
//----------------------------------------------------------------------------------//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
