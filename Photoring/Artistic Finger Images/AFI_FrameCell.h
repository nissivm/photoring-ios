//
//  AFIPaintCell.h
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 18/12/14.
//  Copyright (c) 2014 App Magic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFI_FrameCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *frameImage;
@property (weak, nonatomic) IBOutlet UIImageView *cellSelection;

@end
