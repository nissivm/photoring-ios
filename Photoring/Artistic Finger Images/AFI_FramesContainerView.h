//
//  AFI_FramesContainerView.h
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 24/01/15.
//  Copyright (c) 2014 App Magic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFI_FramesContainerView : UIViewController

@end
