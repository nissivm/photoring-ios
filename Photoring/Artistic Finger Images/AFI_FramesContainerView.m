//
//  AFI_FramesContainerView.m
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 24/01/15.
//  Copyright (c) 2014 App Magic. All rights reserved.
//

#import "AFI_FramesContainerView.h"
#import "AFI_FrameCell.h"

@interface AFI_FramesContainerView () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation AFI_FramesContainerView
{
    NSArray *framesIcons;
    NSIndexPath *idxPath;
    NSString *imageOrientation;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    framesIcons = [[NSArray alloc] initWithObjects: @"icon_frame_1", @"icon_frame_2", @"icon_frame_3", @"icon_frame_4", @"icon_frame_5", @"icon_frame_6", @"icon_frame_7",
                                                    @"icon_frame_8", @"icon_frame_9", @"icon_frame_10", @"icon_frame_11", @"icon_frame_12", @"icon_frame_13", @"icon_frame_14",
                                                    @"icon_frame_15", @"icon_frame_16", @"icon_frame_17", @"icon_frame_18", @"icon_frame_19", @"icon_frame_20", @"icon_frame_21",
                                                    @"icon_frame_22", @"icon_frame_23", @"icon_frame_24", @"icon_frame_25", @"icon_frame_26", @"icon_frame_27", @"icon_frame_28",
                                                    @"icon_frame_29", @"icon_frame_30", @"icon_frame_31", @"icon_frame_32", @"icon_frame_33", @"icon_frame_34", @"icon_frame_35",
                                                    @"icon_frame_36", @"icon_frame_37", @"icon_frame_38", @"icon_frame_39", @"icon_frame_40", @"icon_frame_41", @"icon_frame_42",
                                                    @"icon_frame_43", @"icon_frame_44", @"icon_frame_45", @"icon_frame_46", @"icon_frame_47", @"icon_frame_48", @"icon_frame_49",
                                                    @"icon_frame_50", nil];
    
    idxPath = [NSIndexPath indexPathForItem:-1 inSection:0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(framesVCExposed:) name:@"FramesVCExposed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeFrame) name:@"InsertScenario" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeFrame) name:@"InsertMask" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeFrame) name:@"WorkTableWasCleaned" object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//----------------------------------------------------------------------------------//
#pragma mark - Notifications
//----------------------------------------------------------------------------------//

- (void)framesVCExposed:(NSNotification *)notification
{
    imageOrientation = notification.object;
    
    if(idxPath.item >= 0) // Sinal que um frame foi escolhido anteriormente
    {
        [self.collectionView scrollToItemAtIndexPath:idxPath
                                    atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                            animated:YES];
    }
}

- (void)removeFrame
{
    if(idxPath.item >= 0)
    {
        idxPath = [NSIndexPath indexPathForItem:-1 inSection:0];
        
        [self.collectionView reloadData];
        
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]
                                    atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                            animated:YES];
    }
}

//----------------------------------------------------------------------------------//
#pragma mark - IBActions
//----------------------------------------------------------------------------------//

- (IBAction)removeFrameButtonTapped:(UIButton *)sender
{
    [self removeFrame];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveFrame"
                                                        object:nil];
}

- (IBAction)closeButtonTapped:(UIButton *)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseContainerView"
                                                        object:nil];
}

//----------------------------------------------------------------------------------//
#pragma mark - UICollectionViewDataSource
//----------------------------------------------------------------------------------//

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return [framesIcons count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AFI_FrameCell *cell = (AFI_FrameCell *)[cv dequeueReusableCellWithReuseIdentifier:@"FrameCell"
                                                                         forIndexPath:indexPath];
    
    NSString *addr = [[NSBundle mainBundle] pathForResource:[framesIcons objectAtIndex:indexPath.item] ofType:@"png"];
    UIImage *image = [UIImage imageWithContentsOfFile:addr];
    
    cell.frameImage.image = image;
    
    if(idxPath.item >= 0)
    {
        if([idxPath compare:indexPath] == NSOrderedSame)
        {
            cell.cellSelection.hidden = NO;
        }
        else
        {
            cell.cellSelection.hidden = YES;
        }
    }
    else
        cell.cellSelection.hidden = YES;
    
    return cell;
}

//----------------------------------------------------------------------------------//
#pragma mark - UICollectionViewDelegateFlowLayout
//----------------------------------------------------------------------------------//

- (CGSize)collectionView:(UICollectionView *)cv
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float cellSide = self.collectionView.frame.size.width/8;
    
    return CGSizeMake(cellSide, cellSide);
}

- (CGFloat)collectionView:(UICollectionView *)cv
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)cv
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

//-------------------------------------------------------------------------//
#pragma mark - UICollectionViewDelegate
//-------------------------------------------------------------------------//

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.item != idxPath.item)
    {
        AFI_FrameCell *cell;
        
        if(idxPath.item >= 0)
        {
            cell = (AFI_FrameCell *)[collectionView cellForItemAtIndexPath:idxPath];
            cell.cellSelection.hidden = YES;
        }
        
        cell = (AFI_FrameCell *)[collectionView cellForItemAtIndexPath:indexPath];
        cell.cellSelection.hidden = NO;
        
        idxPath = indexPath;
        
        NSString *frameToUse = [[framesIcons objectAtIndex:indexPath.item] substringFromIndex:5];
        
        if([imageOrientation isEqualToString:@"portrait"])
        {
            frameToUse = [frameToUse stringByAppendingString:@"_p"];
        }
        else if([imageOrientation isEqualToString:@"square"])
        {
            frameToUse = [frameToUse stringByAppendingString:@"_s"];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"InsertFrame"
                                                            object:frameToUse];
    }
}

//-------------------------------------------------------------------------//
#pragma mark - Memory Warning
//-------------------------------------------------------------------------//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
