//
//  AFI_MaskCell.h
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 29/03/15.
//  Copyright (c) 2015 Nissi Vieira Miranda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFI_MaskCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *maskImage;
@property (weak, nonatomic) IBOutlet UIImageView *cellSelection;

@end
