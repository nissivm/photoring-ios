//
//  AFI_MasksContainerView.m
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 29/03/15.
//  Copyright (c) 2015 Nissi Vieira Miranda. All rights reserved.
//

#import "AFI_MasksContainerView.h"
#import "AFI_MaskCell.h"

@interface AFI_MasksContainerView () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *removeMaskButton;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *inUseColorView;
@property (weak, nonatomic) IBOutlet UIButton *colorsContainerButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@property (weak, nonatomic) IBOutlet UIView *colorsContainerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;

@end

@implementation AFI_MasksContainerView
{
    NSArray *masksIcons;
    NSIndexPath *idxPath;
    NSString *imageOrientation;
    NSString *maskToUse;
    UIColor *blurColor;
    BOOL colorsContViewShowing;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    masksIcons = [[NSArray alloc] initWithObjects: @"icon_mask_1", @"icon_mask_2", @"icon_mask_3", @"icon_mask_4", @"icon_mask_5", @"icon_mask_6",
                                                   @"icon_mask_7", @"icon_mask_8", @"icon_mask_9", @"icon_mask_10", @"icon_mask_11", @"icon_mask_12",
                                                   @"icon_mask_13", @"icon_mask_14", @"icon_mask_15", @"icon_mask_16", @"icon_mask_17", @"icon_mask_18",
                                                   @"icon_mask_19", @"icon_mask_20", @"icon_mask_21", @"icon_mask_22", @"icon_mask_23", @"icon_mask_24",
                                                   @"icon_mask_25", @"icon_mask_26", @"icon_mask_27", @"icon_mask_28", @"icon_mask_29", @"icon_mask_30",
                                                   @"icon_mask_31", @"icon_mask_32", @"icon_mask_33", @"icon_mask_34", @"icon_mask_35", @"icon_mask_36",
                                                   @"icon_mask_37", @"icon_mask_38", @"icon_mask_39", @"icon_mask_40", @"icon_mask_41", @"icon_mask_42",
                                                   @"icon_mask_43", @"icon_mask_44", @"icon_mask_45", @"icon_mask_46", @"icon_mask_47", @"icon_mask_48",
                                                   @"icon_mask_49", @"icon_mask_50", nil];
    
    UIImage *maskingImage = [UIImage imageNamed:@"paintMask.png"];
    UIImageView *maskImageView = [[UIImageView alloc] initWithImage:maskingImage];
    [maskImageView setFrame:[self.inUseColorView bounds]];
    [[self.inUseColorView layer] setMask:[maskImageView layer]];
    
    self.inUseColorView.backgroundColor = [UIColor whiteColor];
    
    self.colorsContainerView.alpha = 0.0f;
    
    idxPath = [NSIndexPath indexPathForItem:-1 inSection:0];
    
    blurColor = [UIColor clearColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(masksVCExposed:) name:@"MasksVCExposed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(colorChanged:) name:@"ColorChanged" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeColorContView) name:@"RemoveColorContView" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeMask) name:@"InsertScenario" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeMask) name:@"InsertFrame" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeMask) name:@"WorkTableWasCleaned" object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//----------------------------------------------------------------------------------//
#pragma mark - Notifications
//----------------------------------------------------------------------------------//

- (void)masksVCExposed:(NSNotification *)notification
{
    imageOrientation = notification.object;
    
    if(idxPath.item >= 0) // Sinal que uma máscara foi escolhida anteriormente
    {
        [self.collectionView scrollToItemAtIndexPath:idxPath
                                    atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                            animated:YES];
    }
}

- (void)colorChanged:(NSNotification *)notification
{
    blurColor = notification.object;
    
    if([blurColor isEqual:[UIColor clearColor]])
        self.inUseColorView.backgroundColor = [UIColor whiteColor];
    else
        self.inUseColorView.backgroundColor = blurColor;
    
    NSArray *maskInfo = [NSArray arrayWithObjects:maskToUse, blurColor, nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"InsertMask"
                                                        object:maskInfo];
}

- (void)removeColorContView
{
    self.bottomConstraint.constant -= 100.0f;
    [self animateConstraints];
}

- (void)removeMask
{
    if(idxPath.item >= 0)
    {
        idxPath = [NSIndexPath indexPathForItem:-1 inSection:0];
        
        [self.collectionView reloadData];
        
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]
                                    atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                            animated:YES];
    }
}

//----------------------------------------------------------------------------------//
#pragma mark - IBActions
//----------------------------------------------------------------------------------//

- (IBAction)removeMaskButtonTapped:(UIButton *)sender
{
    [self removeMask];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveMask"
                                                        object:nil];
}

- (IBAction)openColorsContainerView:(UIButton *)sender
{
    self.bottomConstraint.constant += 100.0f;
    [self animateConstraints];
}

- (IBAction)closeButtonTapped:(UIButton *)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseContainerView"
                                                        object:nil];
}

//-------------------------------------------------------------------------//
#pragma mark - Animations
//-------------------------------------------------------------------------//

- (void)animateConstraints
{
    [UIView animateWithDuration:0.2f
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:
     ^{
         if(colorsContViewShowing == NO)
         {
             self.colorsContainerView.alpha = 100.0f;
             
             self.removeMaskButton.alpha = 0.0;
             self.collectionView.alpha = 0.0;
             self.inUseColorView.alpha = 0.0;
             self.colorsContainerButton.alpha = 0.0;
             self.closeButton.alpha = 0.0;
         }
         else
         {
             self.colorsContainerView.alpha = 0.0f;
             
             self.removeMaskButton.alpha = 100.0;
             self.collectionView.alpha = 100.0;
             self.inUseColorView.alpha = 100.0;
             self.colorsContainerButton.alpha = 100.0;
             self.closeButton.alpha = 100.0;
         }
         
         [self.view layoutIfNeeded];
     }
     completion:^(BOOL finished)
     {
         if(colorsContViewShowing == NO)
             colorsContViewShowing = YES;
         else
             colorsContViewShowing = NO;
     }];
}

//----------------------------------------------------------------------------------//
#pragma mark - UICollectionViewDataSource
//----------------------------------------------------------------------------------//

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return [masksIcons count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AFI_MaskCell *cell = (AFI_MaskCell *)[cv dequeueReusableCellWithReuseIdentifier:@"MaskCell"
                                                                       forIndexPath:indexPath];
    
    NSString *addr = [[NSBundle mainBundle] pathForResource:[masksIcons objectAtIndex:indexPath.item] ofType:@"png"];
    UIImage *image = [UIImage imageWithContentsOfFile:addr];
    
    cell.maskImage.image = image;
    
    if(idxPath.item >= 0)
    {
        if([idxPath compare:indexPath] == NSOrderedSame)
        {
            cell.cellSelection.hidden = NO;
        }
        else
        {
            cell.cellSelection.hidden = YES;
        }
    }
    else
        cell.cellSelection.hidden = YES;
    
    return cell;
}

//----------------------------------------------------------------------------------//
#pragma mark - UICollectionViewDelegateFlowLayout
//----------------------------------------------------------------------------------//

- (CGSize)collectionView:(UICollectionView *)cv
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float cellSide = self.collectionView.frame.size.width/7;
    
    return CGSizeMake(cellSide, cellSide);
}

- (CGFloat)collectionView:(UICollectionView *)cv
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)cv
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

//-------------------------------------------------------------------------//
#pragma mark - UICollectionViewDelegate
//-------------------------------------------------------------------------//

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.item != idxPath.item)
    {
        AFI_MaskCell *cell;
        
        if(idxPath.item >= 0)
        {
            cell = (AFI_MaskCell *)[collectionView cellForItemAtIndexPath:idxPath];
            cell.cellSelection.hidden = YES;
        }
        
        cell = (AFI_MaskCell *)[collectionView cellForItemAtIndexPath:indexPath];
        cell.cellSelection.hidden = NO;
        
        idxPath = indexPath;
        
        maskToUse = [[masksIcons objectAtIndex:indexPath.item] substringFromIndex:5];
        
        if([imageOrientation isEqualToString:@"portrait"])
        {
            maskToUse = [maskToUse stringByAppendingString:@"_p"];
        }
        else if([imageOrientation isEqualToString:@"square"])
        {
            maskToUse = [maskToUse stringByAppendingString:@"_s"];
        }
        
        NSArray *maskInfo = [NSArray arrayWithObjects:maskToUse, blurColor, nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"InsertMask"
                                                            object:maskInfo];
    }
}

//-------------------------------------------------------------------------//
#pragma mark - Memory Warning
//-------------------------------------------------------------------------//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

@end
