//
//  AFIPhotoViewController.m
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 28/12/14.
//  Copyright (c) 2014 Nissi Vieira Miranda. All rights reserved.
//

#import "AFI_PhotoViewController.h"
#import "AFI_ClasseAuxiliar.h"

@interface AFI_PhotoViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *cameraFailAlertView;
@property (weak, nonatomic) IBOutlet UIView *panoramicImageAlertView;
@property (weak, nonatomic) IBOutlet UIView *narrowImageAlertView;

@end

@implementation AFI_PhotoViewController
{
    UIPopoverController *popoverController;
    AFI_ClasseAuxiliar *auxiliar;
    NSUserDefaults *userDefaults;
    
    BOOL pushedViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    auxiliar = [AFI_ClasseAuxiliar sharedAuxiliar];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground)
                                                 name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    auxiliar.maxStageWidth = self.view.frame.size.width - 40.0f;
    auxiliar.maxStageHeight = self.view.frame.size.height - (self.view.frame.size.height * 0.1042) - 40.0f;
}

//-------------------------------------------------------------------------//
#pragma mark - Notifications
//-------------------------------------------------------------------------//

- (void)appDidEnterBackground
{
    if(pushedViewController)
    {
        pushedViewController = FALSE;
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:UIApplicationDidReceiveMemoryWarningNotification
         object:Nil];
    }
    
    //NSLog(@"appDidEnterBackground - Photo viewcontroller");
}

//-------------------------------------------------------------------------//
#pragma mark - IBActions
//-------------------------------------------------------------------------//

- (IBAction)cameraButtonTapped:(UIButton *)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.allowsEditing = NO;
        picker.modalPresentationStyle = UIModalPresentationFullScreen;
        
        [self presentViewController:picker animated:YES completion:nil];
    }
    else
    {
        self.cameraFailAlertView.hidden = NO;
    }
}

- (IBAction)photoAlbumButtonTapped:(UIButton *)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.allowsEditing = NO;
    
    popoverController = [[UIPopoverController alloc] initWithContentViewController:picker];
    popoverController.delegate = self;
    [popoverController presentPopoverFromRect:CGRectMake(sender.frame.size.width/2, sender.frame.size.height/2, 1, 1)
                                       inView:sender
                     permittedArrowDirections:UIPopoverArrowDirectionLeft
                                     animated:YES];
}

- (IBAction)okButtonTapped:(UIButton *)sender
{
    self.cameraFailAlertView.hidden = YES;
    self.panoramicImageAlertView.hidden = YES;
    self.narrowImageAlertView.hidden = YES;
}

//-------------------------------------------------------------------------//
#pragma mark - UIImagePickerControllerDelegate
//-------------------------------------------------------------------------//

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *pickerImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [popoverController dismissPopoverAnimated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    if((pickerImage.size.width > pickerImage.size.height) &&
       (pickerImage.size.width/pickerImage.size.height > 1.91))
    {
        self.panoramicImageAlertView.hidden = NO;
    }
    else if((pickerImage.size.width < pickerImage.size.height) &&
            (pickerImage.size.height/pickerImage.size.width > 1.51))
    {
        self.narrowImageAlertView.hidden = NO;
    }
    else
    {        
        NSData *imageData = UIImageJPEGRepresentation([auxiliar getRedimensionedImage:pickerImage], 0.8);
        
        [userDefaults setObject:imageData forKey:@"originalImage"];
        [userDefaults synchronize];
        
        pushedViewController = TRUE;
        
        [self performSegueWithIdentifier:@"ToWorkTableVC" sender:Nil];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [popoverController dismissPopoverAnimated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

//-------------------------------------------------------------------------//
#pragma mark - Memory Warning
//-------------------------------------------------------------------------//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    NSLog(@"didReceiveMemoryWarning");
}

@end
