//
//  AFI_ScenarioCell.h
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 20/03/15.
//  Copyright (c) 2015 Nissi Vieira Miranda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFI_ScenarioCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *scenarioImage;
@property (weak, nonatomic) IBOutlet UIImageView *cellSelection;

@end
