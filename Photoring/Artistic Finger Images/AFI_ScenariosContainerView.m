//
//  AFI_ScenariosContainerView.m
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 20/03/15.
//  Copyright (c) 2015 Nissi Vieira Miranda. All rights reserved.
//

#import "AFI_ScenariosContainerView.h"
#import "AFI_ScenarioCell.h"

@interface AFI_ScenariosContainerView () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation AFI_ScenariosContainerView
{
    NSArray *scenariosIcons;
    NSIndexPath *idxPath;
    NSString *imageOrientation;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    scenariosIcons = [[NSArray alloc] initWithObjects: @"icon_scenario_1", @"icon_scenario_2", @"icon_scenario_3", @"icon_scenario_4", @"icon_scenario_5", @"icon_scenario_6",
                                                       @"icon_scenario_7", @"icon_scenario_8", @"icon_scenario_9", @"icon_scenario_10", @"icon_scenario_11", @"icon_scenario_12",
                                                       @"icon_scenario_13", @"icon_scenario_14", @"icon_scenario_15", @"icon_scenario_16", @"icon_scenario_17", @"icon_scenario_18",
                                                       @"icon_scenario_19", @"icon_scenario_20", @"icon_scenario_21", @"icon_scenario_22", @"icon_scenario_23", @"icon_scenario_24",
                                                       @"icon_scenario_25", @"icon_scenario_26", @"icon_scenario_27", @"icon_scenario_28", @"icon_scenario_29", @"icon_scenario_30",
                                                       @"icon_scenario_31", @"icon_scenario_32", @"icon_scenario_33", @"icon_scenario_34", @"icon_scenario_35", @"icon_scenario_36",
                                                       @"icon_scenario_37", @"icon_scenario_38", @"icon_scenario_39", @"icon_scenario_40", @"icon_scenario_41", @"icon_scenario_42",
                                                       @"icon_scenario_43", @"icon_scenario_44", @"icon_scenario_45", @"icon_scenario_46", @"icon_scenario_47", @"icon_scenario_48",
                                                       @"icon_scenario_49", @"icon_scenario_50", nil];
    
    idxPath = [NSIndexPath indexPathForItem:-1 inSection:0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scenariosVCExposed:) name:@"ScenariosVCExposed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeScenario) name:@"InsertFrame" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeScenario) name:@"InsertMask" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeScenario) name:@"WorkTableWasCleaned" object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//----------------------------------------------------------------------------------//
#pragma mark - Notifications
//----------------------------------------------------------------------------------//

- (void)scenariosVCExposed:(NSNotification *)notification
{
    imageOrientation = notification.object;
    
    if(idxPath.item >= 0) // Sinal que um scenario foi escolhido anteriormente
    {
        [self.collectionView scrollToItemAtIndexPath:idxPath
                                    atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                            animated:YES];
    }
}

- (void)removeScenario
{
    if(idxPath.item >= 0)
    {
        idxPath = [NSIndexPath indexPathForItem:-1 inSection:0];
        
        [self.collectionView reloadData];
        
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]
                                    atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                            animated:YES];
    }
}

//----------------------------------------------------------------------------------//
#pragma mark - IBActions
//----------------------------------------------------------------------------------//

- (IBAction)removeScenarioButtonTapped:(UIButton *)sender
{
    [self removeScenario];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveScenario"
                                                        object:nil];
}

- (IBAction)closeButtonTapped:(UIButton *)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseContainerView"
                                                        object:nil];
}

//----------------------------------------------------------------------------------//
#pragma mark - UICollectionViewDataSource
//----------------------------------------------------------------------------------//

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return [scenariosIcons count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    AFI_ScenarioCell *cell = (AFI_ScenarioCell *)[cv dequeueReusableCellWithReuseIdentifier:@"ScenarioCell"
                                                                               forIndexPath:indexPath];
    
    NSString *addr = [[NSBundle mainBundle] pathForResource:[scenariosIcons objectAtIndex:indexPath.item] ofType:@"png"];
    UIImage *image = [UIImage imageWithContentsOfFile:addr];
    
    cell.scenarioImage.image = image;
    
    if(idxPath.item >= 0)
    {
        if([idxPath compare:indexPath] == NSOrderedSame)
        {
            cell.cellSelection.hidden = NO;
        }
        else
        {
            cell.cellSelection.hidden = YES;
        }
    }
    else
        cell.cellSelection.hidden = YES;
    
    return cell;
}

//----------------------------------------------------------------------------------//
#pragma mark - UICollectionViewDelegateFlowLayout
//----------------------------------------------------------------------------------//

- (CGSize)collectionView:(UICollectionView *)cv
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float cellSide = self.collectionView.frame.size.width/8;
    
    return CGSizeMake(cellSide, cellSide);
}

- (CGFloat)collectionView:(UICollectionView *)cv
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)cv
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

//-------------------------------------------------------------------------//
#pragma mark - UICollectionViewDelegate
//-------------------------------------------------------------------------//

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.item != idxPath.item)
    {
        AFI_ScenarioCell *cell;
        
        if(idxPath.item >= 0)
        {
            cell = (AFI_ScenarioCell *)[collectionView cellForItemAtIndexPath:idxPath];
            cell.cellSelection.hidden = YES;
        }
        
        cell = (AFI_ScenarioCell *)[collectionView cellForItemAtIndexPath:indexPath];
        cell.cellSelection.hidden = NO;
        
        idxPath = indexPath;
        
        NSString *scenarioToUse = [[scenariosIcons objectAtIndex:indexPath.item] substringFromIndex:5];
        
        if([imageOrientation isEqualToString:@"portrait"])
        {
            scenarioToUse = [scenarioToUse stringByAppendingString:@"_p"];
        }
        else if([imageOrientation isEqualToString:@"square"])
        {
            scenarioToUse = [scenarioToUse stringByAppendingString:@"_s"];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"InsertScenario"
                                                            object:scenarioToUse];
    }
}

//-------------------------------------------------------------------------//
#pragma mark - Memory Warning
//-------------------------------------------------------------------------//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
