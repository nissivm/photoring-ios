//
//  AFI_ShareContainerView.m
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 04/01/15.
//  Copyright (c) 2015 Nissi Vieira Miranda. All rights reserved.
//

#import "AFI_ShareContainerView.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"

@interface AFI_ShareContainerView ()

@property (weak, nonatomic) IBOutlet UIButton *saveImageButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIView *successAlertView;
@property (weak, nonatomic) IBOutlet UIView *errorAlertView;

@property (strong, atomic) ALAssetsLibrary *library;

@end

@implementation AFI_ShareContainerView
{
    UIImage *imageToShare;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.library = [[ALAssetsLibrary alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageToShare:) name:@"ImageToShare" object:nil];
}

//-------------------------------------------------------------------------//
#pragma mark - Notifications
//-------------------------------------------------------------------------//

- (void)imageToShare:(NSNotification *)notification
{
    imageToShare = notification.object;
    self.saveImageButton.hidden = NO;
    self.cancelButton.hidden = NO;
}

//-------------------------------------------------------------------------//
#pragma mark - IBActions
//-------------------------------------------------------------------------//

- (IBAction)saveImageButtonTapped:(UIButton *)sender
{
    self.saveImageButton.hidden = YES;
    self.cancelButton.hidden = YES;
    
    void (^completion)(NSURL *, NSError *) = ^(NSURL *assetURL, NSError *error)
    {
        if(error != nil)
        {
            self.errorAlertView.hidden = NO;
            
            NSString *errorMessage = [NSString stringWithFormat:@"%@", [error description]];
            NSLog(@"Error when saving image: %@", errorMessage);
        }
        else
        {
            self.successAlertView.hidden = NO;
        }
    };
    
    void (^failure)(NSError *) = ^(NSError *error)
    {
        if(error != nil)
        {
            self.errorAlertView.hidden = NO;
            
            NSString *errorMessage = [NSString stringWithFormat:@"%@", [error description]];
            NSLog(@"Error when saving image: %@", errorMessage);
        }
    };
    
    // Save image to custom photo album:
    [self.library saveImage:imageToShare
                    toAlbum:@"Photoring"
                 completion:completion
                    failure:failure];
}

- (IBAction)cancelButtonTapped:(UIButton *)sender
{
    imageToShare = Nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseContainerView"
                                                        object:nil];
    self.saveImageButton.hidden = YES;
    self.cancelButton.hidden = YES;
}

- (IBAction)okButtonTapped:(UIButton *)sender
{
    imageToShare = Nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseContainerView"
                                                        object:nil];
    self.successAlertView.hidden = YES;
    self.errorAlertView.hidden = YES;
}

//-------------------------------------------------------------------------//
#pragma mark - Memory Warning
//-------------------------------------------------------------------------//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

@end
