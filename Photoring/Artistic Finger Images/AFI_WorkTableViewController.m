//
//  AFIDrawingViewController.m
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 18/12/14.
//  Copyright (c) 2014 App Magic. All rights reserved.
//

#import "AFI_WorkTableViewController.h"
#import "GPUImage.h"
#import "AFI_ClasseAuxiliar.h"
#import "UIColor+HexToUIColor.h"
#import "FXBlurView.h"

@interface AFI_WorkTableViewController ()

@property (weak, nonatomic) IBOutlet UIButton *startOverButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;

@property (weak, nonatomic) IBOutlet UIView *gpuImageView;
@property (weak, nonatomic) IBOutlet UIView *toolbar;
@property (weak, nonatomic) IBOutlet UIView *filtersContainerView;
@property (weak, nonatomic) IBOutlet UIView *shareContainerView;
@property (weak, nonatomic) IBOutlet UIView *framesContainerView;
@property (weak, nonatomic) IBOutlet UIView *scenariosContainerView;
@property (weak, nonatomic) IBOutlet UIView *masksContainerView;

@property (weak, nonatomic) IBOutlet UIView *spinnerBackground;
@property (weak, nonatomic) IBOutlet UIView *processingView;

@property (strong, nonatomic) UIImage *imagemFiltrada;

@end

@implementation AFI_WorkTableViewController
{
    AFI_ClasseAuxiliar *auxiliar;
    UIView *contViewShowing;
    NSUserDefaults *userDefaults;
    BOOL isRetina;
    
    NSString *selectedFrame;
    NSString *selectedScenario;
    
    NSString *maskToUse;
    UIColor *blurColor;
    
    UIImageView *frameLayer;
    UIImageView *scenarioLayer;
    UIImageView *maskLayer;
    
    UIImageView *origImageHolder;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    auxiliar = [AFI_ClasseAuxiliar sharedAuxiliar];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.imagemFiltrada = Nil;
    
    // Para contornar o fato de que nenhuma imagem é exibida inicialmente na gpuImageView:
    UIImage *imagemOriginal = [UIImage imageWithData:[userDefaults objectForKey:@"originalImage"]];
    origImageHolder = [[UIImageView alloc] initWithFrame:self.gpuImageView.frame];
    origImageHolder.contentMode = UIViewContentModeScaleAspectFit;
    origImageHolder.backgroundColor = [UIColor clearColor];
    origImageHolder.image = imagemOriginal;
    [self.view insertSubview:origImageHolder aboveSubview:self.gpuImageView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground)
                                                 name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground)
                                                 name:UIApplicationWillEnterForegroundNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeContainerView) name:@"CloseContainerView" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(insertFrame:) name:@"InsertFrame" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeFrame) name:@"RemoveFrame" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(insertScenario:) name:@"InsertScenario" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeScenario) name:@"RemoveScenario" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(insertMask:) name:@"InsertMask" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeMask) name:@"RemoveMask" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(lookupFilter:) name:@"LookupFilter" object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    if([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [UIScreen mainScreen].scale > 1)
    {
        isRetina = YES;
    }
    else
        isRetina = NO;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//-------------------------------------------------------------------------//
#pragma mark - Notifications
//-------------------------------------------------------------------------//

- (void)appDidEnterBackground
{
    if(frameLayer)
    {
        selectedScenario = nil;
        maskToUse = nil;
    }
    else if(scenarioLayer)
    {
        selectedFrame = nil;
        maskToUse = nil;
    }
    else if(maskLayer)
    {
        selectedScenario = nil;
        selectedFrame = nil;
    }
    else
    {
        selectedScenario = nil;
        selectedFrame = nil;
        maskToUse = nil;
    }
    
    [self cleanWorkTable];
    
    if(origImageHolder)
    {
        origImageHolder.hidden = NO;
    }
    else
    {
        self.gpuImageView.hidden = NO;
    }
    
    //NSLog(@"appDidEnterBackground - Work table viewcontroller");
}

- (void)appWillEnterForeground
{
    if(selectedFrame)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"InsertFrame"
                                                            object:selectedFrame];
    }
    else if(selectedScenario)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"InsertScenario"
                                                            object:selectedScenario];
    }
    else if(maskToUse)
    {
        NSArray *maskInfo = [NSArray arrayWithObjects:maskToUse, blurColor, nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"InsertMask"
                                                            object:maskInfo];
    }
    
    //NSLog(@"appWillEnterForeground");
}

- (void)closeContainerView
{
    contViewShowing.hidden = YES;
    
    if(self.toolbar.hidden)
        self.toolbar.hidden = NO;
}

// Frames related notifications:

- (void)insertFrame:(NSNotification *)notification
{
    selectedFrame = notification.object;
    
    [self cleanWorkTable];
    
    CGRect frameToUse = [self findOutFrameToUse_frameInserting];
    
    frameLayer = [[UIImageView alloc] initWithFrame:frameToUse];
    frameLayer.contentMode = UIViewContentModeScaleAspectFit;
    frameLayer.backgroundColor = [UIColor clearColor];
    
    frameToUse = CGRectMake(0, 0, frameToUse.size.width, frameToUse.size.height);
    
    // Image Layer:
    
    UIImage *backImage;
    
    if(self.imagemFiltrada)
        backImage = self.imagemFiltrada;
    else
        backImage = [UIImage imageWithData:[userDefaults objectForKey:@"originalImage"]];
    
    UIImageView *layer_01 = [[UIImageView alloc] initWithFrame:frameToUse];
    layer_01.contentMode = UIViewContentModeScaleAspectFit;
    layer_01.backgroundColor = [UIColor clearColor];
    layer_01.image = [auxiliar getImageUnderProportion:backImage];
    
    // Frame Layer:
    
    NSString *frameAddr = [[NSBundle mainBundle] pathForResource:selectedFrame ofType:@"png"];
    UIImage *frameImage = [UIImage imageWithContentsOfFile:frameAddr];
    
    if(isRetina == NO)
        frameImage = [auxiliar createNonRetinaImage:frameImage];
    
    UIImageView *layer_02 = [[UIImageView alloc] initWithFrame:frameToUse];
    layer_02.contentMode = UIViewContentModeScaleAspectFit;
    layer_02.backgroundColor = [UIColor clearColor];
    layer_02.image = frameImage;
    
    // Container:
    
    UIView *container = [[UIView alloc] initWithFrame:frameToUse];
    container.backgroundColor = [UIColor clearColor];
    [container addSubview:layer_01];
    [container addSubview:layer_02];
    
    frameLayer.image = [self genImageFromView:container];
    
    [self.view insertSubview:frameLayer aboveSubview:self.gpuImageView];
    
    [self checkShareStartOverButtonsEnability];
}

- (void)removeFrame
{
    if(frameLayer)
    {
        [frameLayer removeFromSuperview];
        frameLayer = Nil;
        
        if(origImageHolder)
        {
            origImageHolder.hidden = NO;
        }
        else
        {
            self.gpuImageView.hidden = NO;
        }
        
        [self checkShareStartOverButtonsEnability];
    }
}

//Scenarios related notifications:

- (void)insertScenario:(NSNotification *)notification
{
    selectedScenario = notification.object;
    
    [self cleanWorkTable];
    
    CGRect frameToUse = [self findOutFrameToUse];
    
    scenarioLayer = [[UIImageView alloc] initWithFrame:frameToUse];
    scenarioLayer.contentMode = UIViewContentModeScaleAspectFit;
    scenarioLayer.backgroundColor = [UIColor clearColor];
    
    frameToUse = CGRectMake(0, 0, frameToUse.size.width, frameToUse.size.height);
    
    // Image Layer:
    
    UIImage *backImage;
    
    if(self.imagemFiltrada)
        backImage = self.imagemFiltrada;
    else
        backImage = [UIImage imageWithData:[userDefaults objectForKey:@"originalImage"]];
    
    UIImageView *layer_01 = [[UIImageView alloc] initWithFrame:frameToUse];
    layer_01.contentMode = UIViewContentModeScaleAspectFit;
    layer_01.backgroundColor = [UIColor clearColor];
    layer_01.image = [auxiliar getImageUnderProportion:backImage];
    
    // Scenario Layer:
    
    NSString *scenarioAddr = [[NSBundle mainBundle] pathForResource:selectedScenario ofType:@"png"];
    UIImage *scenarioImage = [UIImage imageWithContentsOfFile:scenarioAddr];
    
    if(isRetina == NO)
        scenarioImage = [auxiliar createNonRetinaImage:scenarioImage];
    
    UIImageView *layer_02 = [[UIImageView alloc] initWithFrame:frameToUse];
    layer_02.contentMode = UIViewContentModeScaleAspectFit;
    layer_02.backgroundColor = [UIColor clearColor];
    layer_02.image = scenarioImage;
    
    // Container:
    
    UIView *container = [[UIView alloc] initWithFrame:frameToUse];
    container.backgroundColor = [UIColor clearColor];
    [container addSubview:layer_01];
    [container addSubview:layer_02];
    
    scenarioLayer.image = [self genImageFromView:container];
    
    [self.view insertSubview:scenarioLayer aboveSubview:self.gpuImageView];
    
    [self checkShareStartOverButtonsEnability];
}

- (void)removeScenario
{
    if(scenarioLayer)
    {
        [scenarioLayer removeFromSuperview];
        scenarioLayer = Nil;
        
        if(origImageHolder)
        {
            origImageHolder.hidden = NO;
        }
        else
        {
            self.gpuImageView.hidden = NO;
        }
        
        [self checkShareStartOverButtonsEnability];
    }
}

//Masks related notifications:

- (void)insertMask:(NSNotification *)notification
{
    self.spinnerBackground.hidden = NO;
    self.processingView.hidden = NO;
    
    NSArray *maskInfo = notification.object;
    maskToUse = [maskInfo objectAtIndex:0];
    blurColor = [maskInfo objectAtIndex:1];
    
    CGRect frameToUse = [self findOutFrameToUse];
    frameToUse = CGRectMake(0, 0, frameToUse.size.width, frameToUse.size.height);
    
    // Image Layer:
    UIImage *imageToMask;
    
    if(self.imagemFiltrada)
        imageToMask = self.imagemFiltrada;
    else
        imageToMask = [UIImage imageWithData:[userDefaults objectForKey:@"originalImage"]];
    
    UIImageView *layer_01 = [[UIImageView alloc] initWithFrame:frameToUse];
    layer_01.contentMode = UIViewContentModeScaleAspectFit;
    layer_01.backgroundColor = [UIColor clearColor];
    layer_01.image = [auxiliar getImageUnderProportion:imageToMask];
    
    // Blurred view layer:
    
    FXBlurView *layer_02 = [[FXBlurView alloc] initWithFrame:frameToUse];
    layer_02.dynamic = NO;
    
    // Color layer:
    
    UIView *layer_03;
    if([blurColor isEqual:[UIColor clearColor]] == NO)
    {
        layer_03 = [[UIView alloc] initWithFrame:frameToUse];
        layer_03.backgroundColor = blurColor;
        layer_03.alpha = 0.5;
    }
    
    // Masked image layer:
    
    NSString *maskAddr = [[NSBundle mainBundle] pathForResource:maskToUse ofType:@"png"];
    UIImage *maskImage = [UIImage imageWithContentsOfFile:maskAddr];
    
    if(isRetina == NO)
        maskImage = [auxiliar createNonRetinaImage:maskImage];
    
    UIImageView *layer_04 = [[UIImageView alloc] initWithFrame:frameToUse];
    layer_04.contentMode = UIViewContentModeScaleAspectFit;
    layer_04.backgroundColor = [UIColor clearColor];
    
    __block UIImage *maskedImage;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        maskedImage = [auxiliar maskImage:layer_01.image withMask:maskImage];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            layer_04.image = maskedImage;
            
            // Container:
            
            UIView *container = [[UIView alloc] initWithFrame:frameToUse];
            container.backgroundColor = [UIColor clearColor];
            [container addSubview:layer_01];
            [container addSubview:layer_02];
            if(layer_03)
                [container addSubview:layer_03];
            [container addSubview:layer_04];
            
            [self cleanWorkTable];
            
            CGRect frameToUse = [self findOutFrameToUse];
            
            maskLayer = [[UIImageView alloc] initWithFrame:frameToUse];
            maskLayer.contentMode = UIViewContentModeScaleAspectFit;
            maskLayer.backgroundColor = [UIColor clearColor];
            maskLayer.image = [self genImageFromView:container];
            
            [self.view insertSubview:maskLayer aboveSubview:self.gpuImageView];
            
            [self checkShareStartOverButtonsEnability];
            
            self.spinnerBackground.hidden = YES;
            self.processingView.hidden = YES;
        });
    });
}

- (void)removeMask
{
    if(maskLayer)
    {
        [maskLayer removeFromSuperview];
        maskLayer = Nil;
        
        if(origImageHolder)
        {
            origImageHolder.hidden = NO;
        }
        else
        {
            self.gpuImageView.hidden = NO;
        }
        
        [self checkShareStartOverButtonsEnability];
    }
}

// Filters related notification:

- (void)lookupFilter:(NSNotification *)notification
{
    // Após a aplicação do 1o filtro, e mesmo após remover filtro,
    // a imagem passa a ser exibida em gpuImageView, e origImageHolder
    // passa a ser desnecessária:
    if(origImageHolder)
    {
        [origImageHolder removeFromSuperview];
        origImageHolder = Nil;
    }
    
    if(notification.object == Nil)
        self.imagemFiltrada = Nil;
    else
    {
        UIImage *imagemOriginal = [UIImage imageWithData:[userDefaults objectForKey:@"originalImage"]];
        
        GPUImageOutput<GPUImageInput> *lookupFilter = notification.object;
        [lookupFilter useNextFrameForImageCapture];
        self.imagemFiltrada = [lookupFilter imageFromCurrentFramebuffer];
        self.imagemFiltrada = [UIImage imageWithCGImage:self.imagemFiltrada.CGImage
                                                  scale:imagemOriginal.scale
                                            orientation:imagemOriginal.imageOrientation];
    }
    
    if(frameLayer)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"InsertFrame"
                                                            object:selectedFrame];
    }
    else if(scenarioLayer)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"InsertScenario"
                                                            object:selectedScenario];
    }
    else if(maskLayer)
    {
        NSArray *maskInfo = [NSArray arrayWithObjects:maskToUse, blurColor, nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"InsertMask"
                                                            object:maskInfo];
    }
    
    [self checkShareStartOverButtonsEnability];
}

//-------------------------------------------------------------------------//
#pragma mark - IBActions
//-------------------------------------------------------------------------//

- (IBAction)backButtonTapped:(UIButton *)sender
{
    [userDefaults removeObjectForKey:@"originalImage"];
    [userDefaults synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)startOverButtonTapped:(UIButton *)sender
{
    if(self.startOverButton.enabled)
    {
        [self cleanWorkTable];
        
        if(origImageHolder)
        {
            origImageHolder.hidden = NO;
        }
        else
        {
            self.gpuImageView.hidden = NO;
        }
        
        [self checkShareStartOverButtonsEnability];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"WorkTableWasCleaned"
                                                            object:Nil];
    }
}

- (IBAction)filtersButtonTapped:(UIButton *)sender
{
    self.toolbar.hidden = YES;
    contViewShowing = self.filtersContainerView;
    contViewShowing.hidden = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FiltersVCExposed"
                                                        object:Nil];
}

- (IBAction)masksButtonTapped:(UIButton *)sender
{
    self.toolbar.hidden = YES;
    contViewShowing = self.masksContainerView;
    contViewShowing.hidden = NO;
    
    NSString *imageOrientation = [auxiliar getImageOrientation:[UIImage imageWithData:[userDefaults objectForKey:@"originalImage"]]];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MasksVCExposed"
                                                        object:imageOrientation];
}

- (IBAction)scenariosButtonTapped:(UIButton *)sender
{
    self.toolbar.hidden = YES;
    contViewShowing = self.scenariosContainerView;
    contViewShowing.hidden = NO;
    
    NSString *imageOrientation = [auxiliar getImageOrientation:[UIImage imageWithData:[userDefaults objectForKey:@"originalImage"]]];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ScenariosVCExposed"
                                                        object:imageOrientation];
}

- (IBAction)framesButtonTapped:(UIButton *)sender
{
    self.toolbar.hidden = YES;
    contViewShowing = self.framesContainerView;
    contViewShowing.hidden = NO;
    
    NSString *imageOrientation = [auxiliar getImageOrientation:[UIImage imageWithData:[userDefaults objectForKey:@"originalImage"]]];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FramesVCExposed"
                                                            object:imageOrientation];
}

- (IBAction)shareButtonTapped:(UIButton *)sender
{
    if(self.shareButton.enabled)
    {
        [self generateAndPostImageToShare];
        
        self.toolbar.hidden = YES;
        contViewShowing = self.shareContainerView;
        contViewShowing.hidden = NO;
    }
}

//-------------------------------------------------------------------------//
#pragma mark - Auxiliar Methods
//-------------------------------------------------------------------------//

- (void)cleanWorkTable
{
    if(origImageHolder)
    {
        origImageHolder.hidden = YES;
    }
    else
    {
        self.gpuImageView.hidden = YES;
    }
    
    if(scenarioLayer)
    {
        [scenarioLayer removeFromSuperview];
        scenarioLayer = Nil;
    }
    
    if(frameLayer)
    {
        [frameLayer removeFromSuperview];
        frameLayer = Nil;
    }
    
    if(maskLayer)
    {
        [maskLayer removeFromSuperview];
        maskLayer = Nil;
    }
}

// Usado somente com scenarios e masks:

- (CGRect)findOutFrameToUse
{
    double landWidth = self.gpuImageView.frame.size.height * 1.52;
    double landX = (self.view.frame.size.width - landWidth)/2;
    
    double portWidth = self.gpuImageView.frame.size.height/1.35;
    double portX = (self.view.frame.size.width - portWidth)/2;
    
    double squareWidth = self.gpuImageView.frame.size.height;
    double squareX = (self.view.frame.size.width - squareWidth)/2;
    
    CGRect landscapeFrame = CGRectMake(landX, self.gpuImageView.frame.origin.y,
                                       landWidth, self.gpuImageView.frame.size.height);
    
    CGRect portraitFrame = CGRectMake(portX, self.gpuImageView.frame.origin.y,
                                      portWidth, self.gpuImageView.frame.size.height);
    
    CGRect squareFrame = CGRectMake(squareX, self.gpuImageView.frame.origin.y,
                                    squareWidth, self.gpuImageView.frame.size.height);
    
    NSString *imageOrientation = [auxiliar getImageOrientation:[UIImage imageWithData:[userDefaults objectForKey:@"originalImage"]]];
    CGRect frameToUse;
    
    if([imageOrientation isEqualToString:@"landscape"])
    {
        frameToUse = landscapeFrame;
    }
    else if([imageOrientation isEqualToString:@"portrait"])
    {
        frameToUse = portraitFrame;
    }
    else
    {
        frameToUse = squareFrame;
    }
    
    return frameToUse;
}

// Usado com os frames:

- (CGRect)findOutFrameToUse_frameInserting
{
    double portWidth = self.gpuImageView.frame.size.width/2;
    double portX = ((self.view.frame.size.width - portWidth)/2);
    double squareX = ((self.view.frame.size.width - self.gpuImageView.frame.size.height)/2);
    
    CGRect landscapeFrame = self.gpuImageView.frame;
    
    CGRect portraitFrame = CGRectMake(portX, self.gpuImageView.frame.origin.y,
                                      portWidth, self.gpuImageView.frame.size.height);
    
    CGRect squareFrame = CGRectMake(squareX, self.gpuImageView.frame.origin.y,
                                    self.gpuImageView.frame.size.height,
                                    self.gpuImageView.frame.size.height);
    
    NSString *imageOrientation = [auxiliar getImageOrientation:[UIImage imageWithData:[userDefaults objectForKey:@"originalImage"]]];
    CGRect frameToUse;
    
    if([imageOrientation isEqualToString:@"landscape"])
    {
        frameToUse = landscapeFrame;
    }
    else if([imageOrientation isEqualToString:@"portrait"])
    {
        frameToUse = portraitFrame;
    }
    else
    {
        frameToUse = squareFrame;
    }
    
    return frameToUse;
}

- (UIImage *)genImageFromView:(UIView *)view
{
    UIImage *image;
    
    UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, 0.0);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)checkShareStartOverButtonsEnability
{
    if(frameLayer || scenarioLayer || maskLayer)
    {
        self.startOverButton.enabled = YES;
        self.shareButton.enabled = YES;
    }
    else
    {
        self.startOverButton.enabled = NO;
        self.shareButton.enabled = NO;
    }
        
    if(self.imagemFiltrada)
        self.shareButton.enabled = YES;
}

- (void)generateAndPostImageToShare
{
    if(frameLayer)
    {
        UIImage *imagem = [self genImageFromView:frameLayer];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ImageToShare"
                                                            object:imagem];
    }
    else if(scenarioLayer)
    {
        UIImage *imagem = [self genImageFromView:scenarioLayer];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ImageToShare"
                                                            object:imagem];
    }
    else if(maskLayer)
    {
        UIImage *imagem = [self genImageFromView:maskLayer];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ImageToShare"
                                                            object:imagem];
    }
    else
    {
        CGRect rect = CGRectMake(0, 0, self.imagemFiltrada.size.width,
                                        self.imagemFiltrada.size.height);
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:rect];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.backgroundColor = [UIColor clearColor];
        imageView.image = self.imagemFiltrada;
        
        UIImage *imagem = [self genImageFromView:imageView];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ImageToShare"
                                                            object:imagem];
    }
}

//-------------------------------------------------------------------------//
#pragma mark - Memory Warning
//-------------------------------------------------------------------------//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
