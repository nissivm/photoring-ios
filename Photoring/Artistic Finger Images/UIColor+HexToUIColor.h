//
//  UIColor+HexToUIColor.h
//  Not By Car Paris
//
//  Created by Nissi Vieira Miranda on 02/10/14.
//  Copyright (c) 2014 Nissi Vieira Miranda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexToUIColor)

+ (UIColor *)colorWithHex:(UInt32)col;
+ (UIColor *)colorWithHexString:(NSString *)str;

@end
