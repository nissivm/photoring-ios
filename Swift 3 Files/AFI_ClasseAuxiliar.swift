//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  AFI_ClasseAuxiliar.swift
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 25/12/14.
//  Copyright (c) 2014 Nissi Vieira Miranda. All rights reserved.
//
import Foundation
class AFI_ClasseAuxiliar: NSObject {
    var maxStageWidth: Double = 0.0
    var maxStageHeight: Double = 0.0

    class func shared() -> AFI_ClasseAuxiliar {
        if !editor {
            editor = AFI_ClasseAuxiliar()
        }
        return editor
    }

    func getRedimensionedImage(_ image: UIImage) -> UIImage {
        var novaLargura: Double
        var novaAltura: Double
        if image.size.width > image.size.height {
            novaLargura = image.size.width > maxStageWidth ? maxStageWidth : image.size.width
            novaAltura = (novaLargura * image.size.height) / image.size.width
            if novaAltura > maxStageHeight {
                novaAltura = maxStageHeight
                novaLargura = (novaAltura * image.size.width) / image.size.height
            }
        }
        else if image.size.width < image.size.height {
            novaAltura = image.size.height > maxStageHeight ? maxStageHeight : image.size.height
            novaLargura = (novaAltura * image.size.width) / image.size.height
            if novaLargura > maxStageWidth {
                novaLargura = maxStageWidth
                novaAltura = (novaLargura * image.size.height) / image.size.width
            }
        }
        else {
            novaAltura = image.size.height > maxStageHeight ? maxStageHeight : image.size.height
            novaLargura = novaAltura
        }

        UIGraphicsBeginImageContextWithOptions(CGSize(width: CGFloat(novaLargura), height: CGFloat(novaAltura)), true, 0.0)
        image.draw(in: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(novaLargura), height: CGFloat(novaAltura)))
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }

    func createNonRetinaImage(_ image: UIImage) -> UIImage {
        let novaLargura: Double = image.size.width / 2
        let novaAltura: Double = image.size.height / 2
        // Se colocar "YES", a foto fica preta
        UIGraphicsBeginImageContextWithOptions(CGSize(width: CGFloat(novaLargura), height: CGFloat(novaAltura)), false, 0.0)
        image.draw(in: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(novaLargura), height: CGFloat(novaAltura)))
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }

    func getImageUnderProportion(_ image: UIImage) -> UIImage {
        var needsRedimension: Bool = false
        var view: UIView?
        var imageView: UIImageView?
        var novaLargura: Double = 0.0
        var novaAltura: Double = 0.0
        if image.size.width > image.size.height {
            let appProportion: Float = 1.52
            if image.size.width / image.size.height > appProportion {
                needsRedimension = true
                novaLargura = image.size.height * appProportion
                novaAltura = image.size.height
            }
            else if image.size.width / image.size.height < appProportion {
                needsRedimension = true
                novaLargura = image.size.width
                novaAltura = novaLargura / appProportion
            }
        }
        else if image.size.width < image.size.height {
            let appProportion: Float = 1.35
            if image.size.height / image.size.width > appProportion {
                needsRedimension = true
                novaLargura = image.size.width
                novaAltura = image.size.width * appProportion
            }
            else if image.size.height / image.size.width < appProportion {
                needsRedimension = true
                novaAltura = image.size.height
                novaLargura = novaAltura / appProportion
            }
        }

        if needsRedimension {
            imageView = UIImageView(frame: CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(image.size.width), height: CGFloat(image.size.height)))
            imageView?.contentMode = .scaleAspectFit
            imageView?.backgroundColor = UIColor.clear
            imageView?.image = image
            view = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(novaLargura), height: CGFloat(novaAltura)))
            view?.backgroundColor = UIColor.clear
            view?.addSubview(imageView!)
            UIGraphicsBeginImageContextWithOptions(view?.frame?.size, true, 0.0)
            view?.drawHierarchy(in: view?.bounds, afterScreenUpdates: true)
            image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }
        return image
    }

    func maskImage(_ image: UIImage, withMask maskImage: UIImage) -> UIImage {
        let maskRef: CGImageRef? = maskImage.cgImage
        let mask: CGImageRef = CGImageMaskCreate(CGImageGetWidth(maskRef), CGImageGetHeight(maskRef), CGImageGetBitsPerComponent(maskRef), CGImageGetBitsPerPixel(maskRef), CGImageGetBytesPerRow(maskRef), CGImageGetDataProvider(maskRef), nil, false)
        let maskedImageRef: CGImageRef? = image.cgImage.masking(mask)
        let maskedImage = UIImage(cgImage: maskedImageRef)
        CGImageRelease(mask)
        CGImageRelease(maskedImageRef)
        // returns new image with mask applied
        return maskedImage
    }

    func getImageOrientation(_ image: UIImage) -> String {
        let imageWidth: Double = image.size.width
        let imageHeight: Double = image.size.height
        var imageOrientation: String
        if imageWidth > imageHeight {
            imageOrientation = "landscape"
        }
        else if imageWidth < imageHeight {
            imageOrientation = "portrait"
        }
        else {
            imageOrientation = "square"
        }

        return imageOrientation
    }
}

private var editor: AFI_ClasseAuxiliar?