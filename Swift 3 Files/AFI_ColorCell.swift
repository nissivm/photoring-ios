//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  AFI_ColorCell.swift
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 29/03/15.
//  Copyright (c) 2015 Nissi Vieira Miranda. All rights reserved.
//
import UIKit
class AFI_ColorCell: UICollectionViewCell {
    var colorCode: String = ""
    @IBOutlet weak var colorSample: UIView!
}