//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  AFI_ColorsContainerView.swift
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 29/03/15.
//  Copyright (c) 2015 Nissi Vieira Miranda. All rights reserved.
//
import UIKit
class AFI_ColorsContainerView: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var inUseColorView: UIView!

    private var hexColors = [Any]()
    private var hexColorInUse: String = ""

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        hexColors = []
        let data = Data(contentsOfFile: Bundle.main.path(forResource: "colors", ofType: "json"))
        let colorsJson: [AnyHashable: Any]? = try? JSONSerialization.jsonObject(withData: data, options: [])
        let colors: [Any]? = (colorsJson?["colors"] as? [Any])
        for colorSpecs: [AnyHashable: Any] in colors {
            hexColors.append(colorSpecs["colorCode"])
        }
    
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let maskingImage = UIImage(named: "paintMask.png")
        let maskImageView = UIImageView(image: maskingImage)
        maskImageView.frame = inUseColorView.bounds
        inUseColorView.layer().mask = maskImageView.layer()
        inUseColorView.backgroundColor = UIColor.white
        hexColorInUse = (hexColors[hexColors.count - 2] as? String)
    }

    //----------------------------------------------------------------------------------//
// MARK: - IBActions
    //----------------------------------------------------------------------------------//
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        NotificationCenter.default.post(name: "RemoveColorContView", object: nil)
    }

    //----------------------------------------------------------------------------------//
// MARK: - UICollectionViewDataSource
    //----------------------------------------------------------------------------------//
    func numberOfSections(inCollectionView collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ view: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hexColors.count
    }

    func collectionView(_ cv: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: AFI_ColorCell? = (cv.dequeueReusableCell(withReuseIdentifier: "ColorCell", for: indexPath) as? AFI_ColorCell)
        let maskingImage = UIImage(named: "roundMask.png")
        let maskImageView = UIImageView(image: maskingImage)
        maskImageView.frame = cell?.colorSample?.bounds
        cell?.colorSample?.layer()?.mask = maskImageView.layer()
        cell?.colorCode = hexColors[indexPath.item]
        cell?.colorSample?.backgroundColor = UIColor(hexString: cell?.colorCode)
        return cell!
    }

    //----------------------------------------------------------------------------------//
// MARK: - UICollectionViewDelegateFlowLayout
    //----------------------------------------------------------------------------------//
    func collectionView(_ cv: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSide: Float? = collectionView?.frame?.size?.width / 9
        return CGSize(width: CGFloat(cellSide), height: CGFloat(cellSide))
    }

    func collectionView(_ cv: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionat section: Int) -> CGFloat {
        return 0.0
    }

    func collectionView(_ cv: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionat section: Int) -> CGFloat {
        return 0.0
    }

    //-------------------------------------------------------------------------//
// MARK: - UICollectionViewDelegate
    //-------------------------------------------------------------------------//
    func collectionView(_ cv: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell: AFI_ColorCell? = (cv.cellForItem(at: indexPath) as? AFI_ColorCell)
        if (cell?.colorCode == hexColorInUse) == false {
            inUseColorView.backgroundColor = cell?.colorSample?.backgroundColor
            hexColorInUse = cell?.colorCode
            let whiteColor: String? = (hexColors[hexColors.count - 2] as? String)
            if (cell?.colorCode == whiteColor) {
                NotificationCenter.default.post(name: "ColorChanged", object: UIColor.clear)
            }
            else {
                NotificationCenter.default.post(name: "ColorChanged", object: cell?.colorSample?.backgroundColor)
            }
        }
    }

    //-------------------------------------------------------------------------//
// MARK: - Memory Warning
    //-------------------------------------------------------------------------//
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}