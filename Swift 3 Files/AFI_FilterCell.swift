//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  AFI_FilterCell.swift
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 26/12/14.
//  Copyright (c) 2014 Nissi Vieira Miranda. All rights reserved.
//
import UIKit
class AFI_FilterCell: UICollectionViewCell {
    @IBOutlet weak var filterSample: UIImageView!
    @IBOutlet weak var filterSelection: UIImageView!
}