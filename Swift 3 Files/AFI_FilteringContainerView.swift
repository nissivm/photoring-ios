//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  AFIFilteringViewController.h
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 29/12/14.
//  Copyright (c) 2014 Nissi Vieira Miranda. All rights reserved.
//
import UIKit
class AFI_FilteringContainerView: UIViewController {
    private weak var lookupFilter: GPUImageInput?
    private var gpuImageToFilter: GPUImagePicture?
    private var imageView: GPUImageView?
    private var lookupAddr: String = ""
    private var userDefaults: UserDefaults?
    private var hasFilterApplied: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        hasFilterApplied = false
        let mainScreenFrame: CGRect = UIScreen.main.applicationFrame
        let primaryView = GPUImageView(frame: mainScreenFrame)
        view = primaryView
        userDefaults = UserDefaults.standard
        let imagemOriginal = UIImage(data: userDefaults?.object(forKey: "originalImage"))
        lookupFilter = GPUImageLookupFilter()
        gpuImageToFilter = GPUImagePicture(image: imagemOriginal)
        imageView = (view as? GPUImageView)
        imageView?.backgroundColor = UIColor.clear
        imageView?.setBackgroundColorRed(0, green: 0, blue: 0, alpha: 0)
            // Filtragem inicial:
        let filter: GPUImageInput? = GPUImageFilter()
        gpuImageToFilter?.addTarget(filter)
        filter?.addTarget(imageView)
        gpuImageToFilter?.processImage()
        NotificationCenter.default.addObserver(self, selector: #selector(self.applyFilter), name: "ApplyFilter", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeFilters), name: "RemoveFilters", object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    //-------------------------------------------------------------------------//
// MARK: - Notifications
    //-------------------------------------------------------------------------//
    func applyFilter(_ notification: Notification) {
        hasFilterApplied = true
        applyFilter(atIndex: CInt(notification.object))
    }

    func removeFilters() {
        if hasFilterApplied {
            hasFilterApplied = false
            gpuImageToFilter?.removeAllTargets()
            gpuImageToFilter?.addTarget(imageView)
            gpuImageToFilter?.processImage()
            NotificationCenter.default.post(name: "LookupFilter", object: nil)
        }
    }

    //-------------------------------------------------------------------------//
// MARK: - Filters Switch
    //-------------------------------------------------------------------------//
    func applyFilter(at index: Int) {
        index += 1
        switch index {
            case 1:
                lookupAddr = Bundle.main.path(forResource: "Accion", ofType: "png")
            case 2:
                lookupAddr = Bundle.main.path(forResource: "Action16", ofType: "png")
            case 3:
                lookupAddr = Bundle.main.path(forResource: "Aged", ofType: "png")
            case 4:
                lookupAddr = Bundle.main.path(forResource: "Amatorka", ofType: "png")
            case 5:
                lookupAddr = Bundle.main.path(forResource: "Autumn", ofType: "png")
            case 6:
                lookupAddr = Bundle.main.path(forResource: "Bella", ofType: "png")
            case 7:
                lookupAddr = Bundle.main.path(forResource: "BlueEvening", ofType: "png")
            case 8:
                lookupAddr = Bundle.main.path(forResource: "Cinema", ofType: "png")
            case 9:
                lookupAddr = Bundle.main.path(forResource: "ColdWhite", ofType: "png")
            case 10:
                lookupAddr = Bundle.main.path(forResource: "CreamyNoon", ofType: "png")
            case 11:
                lookupAddr = Bundle.main.path(forResource: "CrossProcessing", ofType: "png")
            case 12:
                lookupAddr = Bundle.main.path(forResource: "CyanPastel", ofType: "png")
            case 13:
                lookupAddr = Bundle.main.path(forResource: "FalseColorInfrared", ofType: "png")
            case 14:
                lookupAddr = Bundle.main.path(forResource: "ForRed", ofType: "png")
            case 15:
                lookupAddr = Bundle.main.path(forResource: "GreenishTones", ofType: "png")
            case 16:
                lookupAddr = Bundle.main.path(forResource: "LordKelvin", ofType: "png")
            case 17:
                lookupAddr = Bundle.main.path(forResource: "MagicalEffectOne", ofType: "png")
            case 18:
                lookupAddr = Bundle.main.path(forResource: "MagicalEffectThree", ofType: "png")
            case 19:
                lookupAddr = Bundle.main.path(forResource: "MagicalEffectTwo", ofType: "png")
            case 20:
                lookupAddr = Bundle.main.path(forResource: "MatteBlackWhite", ofType: "png")
            case 21:
                lookupAddr = Bundle.main.path(forResource: "MediumPurple", ofType: "png")
            case 22:
                lookupAddr = Bundle.main.path(forResource: "MidsummerLight", ofType: "png")
            case 23:
                lookupAddr = Bundle.main.path(forResource: "Nashville", ofType: "png")
            case 24:
                lookupAddr = Bundle.main.path(forResource: "OldCamera", ofType: "png")
            case 25:
                lookupAddr = Bundle.main.path(forResource: "Olivia", ofType: "png")
            case 26:
                lookupAddr = Bundle.main.path(forResource: "PaleRed", ofType: "png")
            case 27:
                lookupAddr = Bundle.main.path(forResource: "ParisianRain", ofType: "png")
            case 28:
                lookupAddr = Bundle.main.path(forResource: "PeacefulAutumn", ofType: "png")
            case 29:
                lookupAddr = Bundle.main.path(forResource: "Portrait", ofType: "png")
            case 30:
                lookupAddr = Bundle.main.path(forResource: "Sepia05", ofType: "png")
            case 31:
                lookupAddr = Bundle.main.path(forResource: "StandardInfrared", ofType: "png")
            case 32:
                lookupAddr = Bundle.main.path(forResource: "StandardMono", ofType: "png")
            case 33:
                lookupAddr = Bundle.main.path(forResource: "Steampunk", ofType: "png")
            case 34:
                lookupAddr = Bundle.main.path(forResource: "SunnyDay", ofType: "png")
            case 35:
                lookupAddr = Bundle.main.path(forResource: "VintageBrown", ofType: "png")
            case 36:
                lookupAddr = Bundle.main.path(forResource: "VintageOne", ofType: "png")
            case 37:
                lookupAddr = Bundle.main.path(forResource: "VintageTwo", ofType: "png")
            case 38:
                lookupAddr = Bundle.main.path(forResource: "WarmSpringLight", ofType: "png")
            case 39:
                lookupAddr = Bundle.main.path(forResource: "WashedAway", ofType: "png")
            case 40:
                lookupAddr = Bundle.main.path(forResource: "YellowVintage", ofType: "png")
        }

        let lookupImage = UIImage(contentsOfFile: lookupAddr)
        let lookupGPUImage = GPUImagePicture(image: lookupImage)
        gpuImageToFilter?.addTarget(lookupFilter)
        lookupGPUImage.addTarget(lookupFilter)
        lookupFilter?.addTarget(imageView)
        gpuImageToFilter?.processImage()
        lookupGPUImage.processImage()
        NotificationCenter.default.post(name: "LookupFilter", object: lookupFilter)
    }

    //-------------------------------------------------------------------------//
// MARK: - Memory Warning
    //-------------------------------------------------------------------------//
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//
//  AFI_FilteringViewController.m
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 29/12/14.
//  Copyright (c) 2014 Nissi Vieira Miranda. All rights reserved.
//