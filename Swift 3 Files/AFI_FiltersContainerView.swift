//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  AFIFiltersViewController.h
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 25/12/14.
//  Copyright (c) 2014 Nissi Vieira Miranda. All rights reserved.
//
import UIKit
class AFI_FiltersContainerView: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var collectionView: UICollectionView!

    private var samples = [Any]()
    private var selFilterIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        samples = ["accion_sample", "action16_sample", "aged_sample", "amatorka_sample", "autumn_sample", "bella_sample", "blueEvening_sample", "cinema_sample", "coldWhite_sample", "creamyNoon_sample", "crossProcessing_sample", "cyanPastel_sample", "falseColorInfrared_sample", "forRed_sample", "greenishTones_sample", "lordKelvin_sample", "magicalEffectOne_sample", "magicalEffectThree_sample", "magicalEffectTwo_sample", "matteBlackWhite_sample", "mediumPurple_sample", "midsummerLight_sample", "nashville_sample", "oldCamera_sample", "olivia_sample", "paleRed_sample", "parisianRain_sample", "peacefulAutumn_sample", "portrait_sample", "sepia05_sample", "standardInfrared_sample", "standardMono_sample", "steampunk_sample", "sunnyDay_sample", "vintageBrown_sample", "vintageOne_sample", "vintageTwo_sample", "warmSpringLight_sample", "washedAway_sample", "yellowVintage_sample"]
        selFilterIndex = -1
        NotificationCenter.default.addObserver(self, selector: #selector(self.filtersVCExposed), name: "FiltersVCExposed", object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    //----------------------------------------------------------------------------------//
// MARK: - Notifications
    //----------------------------------------------------------------------------------//
    func filtersVCExposed() {
        if selFilterIndex >= 0 {
            let idxPath = IndexPath(item: selFilterIndex, section: 0)
            collectionView?.scrollToItem(at: idxPath, atScrollPosition: .centeredHorizontally, animated: true)
        }
        else {
            collectionView?.scrollToItem(at: IndexPath(item: 0, section: 0), atScrollPosition: .left, animated: true)
        }
    }

    //----------------------------------------------------------------------------------//
// MARK: - IBActions
    //----------------------------------------------------------------------------------//
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        NotificationCenter.default.post(name: "CloseContainerView", object: nil)
    }

    @IBAction func originalImageButtonTapped(_ sender: UIButton) {
        let idxPath = IndexPath(item: selFilterIndex, section: 0)
        let selectedCell: AFI_FilterCell? = (collectionView?.cellForItem(at: idxPath) as? AFI_FilterCell)
        selectedCell?.filterSelection?.isHidden = true
        selFilterIndex = -1
        NotificationCenter.default.post(name: "RemoveFilters", object: nil)
    }

    //----------------------------------------------------------------------------------//
// MARK: - UICollectionViewDataSource
    //----------------------------------------------------------------------------------//
    func numberOfSections(inCollectionView cv: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ cv: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return samples.count
    }

    func collectionView(_ cv: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: AFI_FilterCell? = (cv.dequeueReusableCell(withReuseIdentifier: "FilterCell", for: indexPath) as? AFI_FilterCell)
        cell?.backgroundColor = UIColor.clear
        let path: String? = Bundle.main.path(forResource: (samples[indexPath.row] as? String), ofType: "png")
        let filterSample = UIImage(contentsOfFile: path)
        cell?.filterSample?.image = filterSample
        if indexPath.row == selFilterIndex {
            cell?.filterSelection?.isHidden = false
        }
        else {
            cell?.filterSelection?.isHidden = true
        }
        return cell!
    }

    //----------------------------------------------------------------------------------//
// MARK: - UICollectionViewDelegateFlowLayout
    //----------------------------------------------------------------------------------//
    func collectionView(_ cv: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSide: Float? = collectionView?.frame?.size?.width / 5
        return CGSize(width: CGFloat(cellSide), height: CGFloat(cellSide))
    }

    func collectionView(_ cv: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionat section: Int) -> CGFloat {
        return 0.0
    }

    func collectionView(_ cv: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionat section: Int) -> CGFloat {
        return 0.0
    }

    //-------------------------------------------------------------------------//
// MARK: - UICollectionViewDelegate
    //-------------------------------------------------------------------------//
    func collectionView(_ cv: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var selectedCell: AFI_FilterCell?
        if selFilterIndex >= 0 {
            let idxPath = IndexPath(item: selFilterIndex, section: 0)
            selectedCell = (cv.cellForItem(at: idxPath) as? AFI_FilterCell)
            selectedCell?.filterSelection?.isHidden = true
        }
        selectedCell = (cv.cellForItem(at: indexPath) as? AFI_FilterCell)
        selectedCell?.filterSelection?.isHidden = false
        selFilterIndex = indexPath.row
        NotificationCenter.default.post(name: "ApplyFilter", object: Int(selFilterIndex))
    }

    //----------------------------------------------------------------------------------//
// MARK: - Memory Warning
    //----------------------------------------------------------------------------------//
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//
//  AFIFiltersViewController.m
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 25/12/14.
//  Copyright (c) 2014 Nissi Vieira Miranda. All rights reserved.
//