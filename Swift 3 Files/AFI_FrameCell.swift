//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  AFIPaintCell.h
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 18/12/14.
//  Copyright (c) 2014 App Magic. All rights reserved.
//
import UIKit
class AFI_FrameCell: UICollectionViewCell {
    @IBOutlet weak var frameImage: UIImageView!
    @IBOutlet weak var cellSelection: UIImageView!
}

//
//  AFIPaintCell.m
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 18/12/14.
//  Copyright (c) 2014 App Magic. All rights reserved.
//