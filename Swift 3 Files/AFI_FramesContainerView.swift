//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  AFI_FramesContainerView.swift
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 24/01/15.
//  Copyright (c) 2014 App Magic. All rights reserved.
//
import UIKit
class AFI_FramesContainerView: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var collectionView: UICollectionView!

    private var framesIcons = [Any]()
    private var idxPath: IndexPath?
    private var imageOrientation: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        framesIcons = ["icon_frame_1", "icon_frame_2", "icon_frame_3", "icon_frame_4", "icon_frame_5", "icon_frame_6", "icon_frame_7", "icon_frame_8", "icon_frame_9", "icon_frame_10", "icon_frame_11", "icon_frame_12", "icon_frame_13", "icon_frame_14", "icon_frame_15", "icon_frame_16", "icon_frame_17", "icon_frame_18", "icon_frame_19", "icon_frame_20", "icon_frame_21", "icon_frame_22", "icon_frame_23", "icon_frame_24", "icon_frame_25", "icon_frame_26", "icon_frame_27", "icon_frame_28", "icon_frame_29", "icon_frame_30", "icon_frame_31", "icon_frame_32", "icon_frame_33", "icon_frame_34", "icon_frame_35", "icon_frame_36", "icon_frame_37", "icon_frame_38", "icon_frame_39", "icon_frame_40", "icon_frame_41", "icon_frame_42", "icon_frame_43", "icon_frame_44", "icon_frame_45", "icon_frame_46", "icon_frame_47", "icon_frame_48", "icon_frame_49", "icon_frame_50"]
        idxPath = IndexPath(item: -1, section: 0)
        NotificationCenter.default.addObserver(self, selector: #selector(self.framesVCExposed), name: "FramesVCExposed", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeFrame), name: "InsertScenario", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeFrame), name: "InsertMask", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeFrame), name: "WorkTableWasCleaned", object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    //----------------------------------------------------------------------------------//
// MARK: - Notifications
    //----------------------------------------------------------------------------------//
    func framesVCExposed(_ notification: Notification) {
        imageOrientation = notification.object
        if idxPath?.item >= 0 {
            collectionView?.scrollToItem(at: idxPath, atScrollPosition: .centeredHorizontally, animated: true)
        }
    }

    func removeFrame() {
        if idxPath?.item >= 0 {
            idxPath = IndexPath(item: -1, section: 0)
            collectionView?.reloadData()
            collectionView?.scrollToItem(at: IndexPath(item: 0, section: 0), atScrollPosition: .centeredHorizontally, animated: true)
        }
    }

    //----------------------------------------------------------------------------------//
// MARK: - IBActions
    //----------------------------------------------------------------------------------//
    @IBAction func removeFrameButtonTapped(_ sender: UIButton) {
        removeFrame()
        NotificationCenter.default.post(name: "RemoveFrame", object: nil)
    }

    @IBAction func closeButtonTapped(_ sender: UIButton) {
        NotificationCenter.default.post(name: "CloseContainerView", object: nil)
    }

    //----------------------------------------------------------------------------------//
// MARK: - UICollectionViewDataSource
    //----------------------------------------------------------------------------------//
    func numberOfSections(inCollectionView collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ view: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return framesIcons.count
    }

    func collectionView(_ cv: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: AFI_FrameCell? = (cv.dequeueReusableCell(withReuseIdentifier: "FrameCell", for: indexPath) as? AFI_FrameCell)
        let addr: String? = Bundle.main.path(forResource: (framesIcons[indexPath.item] as? String), ofType: "png")
        let image = UIImage(contentsOfFile: addr)
        cell?.frameImage?.image = image
        if idxPath?.item >= 0 {
            if idxPath?.compare(indexPath) == .orderedSame {
                cell?.cellSelection?.isHidden = false
            }
            else {
                cell?.cellSelection?.isHidden = true
            }
        }
        else {
            cell?.cellSelection?.isHidden = true
        }
        return cell!
    }

    //----------------------------------------------------------------------------------//
// MARK: - UICollectionViewDelegateFlowLayout
    //----------------------------------------------------------------------------------//
    func collectionView(_ cv: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSide: Float? = collectionView?.frame?.size?.width / 8
        return CGSize(width: CGFloat(cellSide), height: CGFloat(cellSide))
    }

    func collectionView(_ cv: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionat section: Int) -> CGFloat {
        return 0.0
    }

    func collectionView(_ cv: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionat section: Int) -> CGFloat {
        return 0.0
    }

    //-------------------------------------------------------------------------//
// MARK: - UICollectionViewDelegate
    //-------------------------------------------------------------------------//
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item != idxPath?.item {
            var cell: AFI_FrameCell?
            if idxPath?.item >= 0 {
                cell = (collectionView.cellForItem(at: idxPath) as? AFI_FrameCell)
                cell?.cellSelection?.isHidden = true
            }
            cell = (collectionView.cellForItem(at: indexPath) as? AFI_FrameCell)
            cell?.cellSelection?.isHidden = false
            idxPath = indexPath
            var frameToUse: String? = ((framesIcons[indexPath.item] as? String) as? NSString)?.substring(from: 5)
            if (imageOrientation == "portrait") {
                frameToUse = frameToUse! + ("_p")
            }
            else if (imageOrientation == "square") {
                frameToUse = frameToUse! + ("_s")
            }

            NotificationCenter.default.post(name: "InsertFrame", object: frameToUse)
        }
    }

    //-------------------------------------------------------------------------//
// MARK: - Memory Warning
    //-------------------------------------------------------------------------//
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}