//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  AFI_MasksContainerView.swift
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 29/03/15.
//  Copyright (c) 2015 Nissi Vieira Miranda. All rights reserved.
//
import UIKit
class AFI_MasksContainerView: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var removeMaskButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var inUseColorView: UIView!
    @IBOutlet weak var colorsContainerButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var colorsContainerView: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!

    private var masksIcons = [Any]()
    private var idxPath: IndexPath?
    private var imageOrientation: String = ""
    private var maskToUse: String = ""
    private var blurColor: UIColor?
    private var colorsContViewShowing: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        masksIcons = ["icon_mask_1", "icon_mask_2", "icon_mask_3", "icon_mask_4", "icon_mask_5", "icon_mask_6", "icon_mask_7", "icon_mask_8", "icon_mask_9", "icon_mask_10", "icon_mask_11", "icon_mask_12", "icon_mask_13", "icon_mask_14", "icon_mask_15", "icon_mask_16", "icon_mask_17", "icon_mask_18", "icon_mask_19", "icon_mask_20", "icon_mask_21", "icon_mask_22", "icon_mask_23", "icon_mask_24", "icon_mask_25", "icon_mask_26", "icon_mask_27", "icon_mask_28", "icon_mask_29", "icon_mask_30", "icon_mask_31", "icon_mask_32", "icon_mask_33", "icon_mask_34", "icon_mask_35", "icon_mask_36", "icon_mask_37", "icon_mask_38", "icon_mask_39", "icon_mask_40", "icon_mask_41", "icon_mask_42", "icon_mask_43", "icon_mask_44", "icon_mask_45", "icon_mask_46", "icon_mask_47", "icon_mask_48", "icon_mask_49", "icon_mask_50"]
        let maskingImage = UIImage(named: "paintMask.png")
        let maskImageView = UIImageView(image: maskingImage)
        maskImageView.frame = inUseColorView.bounds
        inUseColorView.layer().mask = maskImageView.layer()
        inUseColorView.backgroundColor = UIColor.white
        colorsContainerView.alpha = 0.0
        idxPath = IndexPath(item: -1, section: 0)
        blurColor = UIColor.clear
        NotificationCenter.default.addObserver(self, selector: #selector(self.masksVCExposed), name: "MasksVCExposed", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.colorChanged), name: "ColorChanged", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeColorContView), name: "RemoveColorContView", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeMask), name: "InsertScenario", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeMask), name: "InsertFrame", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeMask), name: "WorkTableWasCleaned", object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    //----------------------------------------------------------------------------------//
// MARK: - Notifications
    //----------------------------------------------------------------------------------//
    func masksVCExposed(_ notification: Notification) {
        imageOrientation = notification.object
        if idxPath?.item >= 0 {
            collectionView?.scrollToItem(at: idxPath, atScrollPosition: .centeredHorizontally, animated: true)
        }
    }

    func colorChanged(_ notification: Notification) {
        blurColor = notification.object
        if blurColor?.isEqual(UIColor.clear) {
            inUseColorView.backgroundColor = UIColor.white
        }
        else {
            inUseColorView.backgroundColor = blurColor
        }
        let maskInfo: [Any] = [maskToUse, blurColor]
        NotificationCenter.default.post(name: "InsertMask", object: maskInfo)
    }

    func removeColorContView() {
        bottomConstraint.constant -= 100.0
        animateConstraints()
    }

    func removeMask() {
        if idxPath?.item >= 0 {
            idxPath = IndexPath(item: -1, section: 0)
            collectionView?.reloadData()
            collectionView?.scrollToItem(at: IndexPath(item: 0, section: 0), atScrollPosition: .centeredHorizontally, animated: true)
        }
    }

    //----------------------------------------------------------------------------------//
// MARK: - IBActions
    //----------------------------------------------------------------------------------//
    @IBAction func removeMaskButtonTapped(_ sender: UIButton) {
        removeMask()
        NotificationCenter.default.post(name: "RemoveMask", object: nil)
    }

    @IBAction func openColorsContainerView(_ sender: UIButton) {
        bottomConstraint.constant += 100.0
        animateConstraints()
    }

    @IBAction func closeButtonTapped(_ sender: UIButton) {
        NotificationCenter.default.post(name: "CloseContainerView", object: nil)
    }

    //-------------------------------------------------------------------------//
// MARK: - Animations
    //-------------------------------------------------------------------------//
    func animateConstraints() {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {() -> Void in
            if self.colorsContViewShowing == false {
                colorsContainerView.alpha = 100.0
                removeMaskButton.alpha = 0.0
                collectionView.alpha = 0.0
                inUseColorView.alpha = 0.0
                colorsContainerButton.alpha = 0.0
                closeButton.alpha = 0.0
            }
            else {
                colorsContainerView.alpha = 0.0
                removeMaskButton.alpha = 100.0
                collectionView.alpha = 100.0
                inUseColorView.alpha = 100.0
                colorsContainerButton.alpha = 100.0
                closeButton.alpha = 100.0
            }
            view.layoutIfNeeded()
        }, completion: {(_ finished: Bool) -> Void in
            if self.colorsContViewShowing == false {
                self.colorsContViewShowing = true
            }
            else {
                self.colorsContViewShowing = false
            }
        })
    }

    //----------------------------------------------------------------------------------//
// MARK: - UICollectionViewDataSource
    //----------------------------------------------------------------------------------//
    func numberOfSections(inCollectionView collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ view: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return masksIcons.count
    }

    func collectionView(_ cv: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: AFI_MaskCell? = (cv.dequeueReusableCell(withReuseIdentifier: "MaskCell", for: indexPath) as? AFI_MaskCell)
        let addr: String? = Bundle.main.path(forResource: (masksIcons[indexPath.item] as? String), ofType: "png")
        let image = UIImage(contentsOfFile: addr)
        cell?.maskImage?.image = image
        if idxPath?.item >= 0 {
            if idxPath?.compare(indexPath) == .orderedSame {
                cell?.cellSelection?.isHidden = false
            }
            else {
                cell?.cellSelection?.isHidden = true
            }
        }
        else {
            cell?.cellSelection?.isHidden = true
        }
        return cell!
    }

    //----------------------------------------------------------------------------------//
// MARK: - UICollectionViewDelegateFlowLayout
    //----------------------------------------------------------------------------------//
    func collectionView(_ cv: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSide: Float? = collectionView?.frame?.size?.width / 7
        return CGSize(width: CGFloat(cellSide), height: CGFloat(cellSide))
    }

    func collectionView(_ cv: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionat section: Int) -> CGFloat {
        return 0.0
    }

    func collectionView(_ cv: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionat section: Int) -> CGFloat {
        return 0.0
    }

    //-------------------------------------------------------------------------//
// MARK: - UICollectionViewDelegate
    //-------------------------------------------------------------------------//
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item != idxPath?.item {
            var cell: AFI_MaskCell?
            if idxPath?.item >= 0 {
                cell = (collectionView.cellForItem(at: idxPath) as? AFI_MaskCell)
                cell?.cellSelection?.isHidden = true
            }
            cell = (collectionView.cellForItem(at: indexPath) as? AFI_MaskCell)
            cell?.cellSelection?.isHidden = false
            idxPath = indexPath
            maskToUse = (masksIcons[indexPath.item] as? NSString)?.substring(from: 5)
            if (imageOrientation == "portrait") {
                maskToUse = maskToUse + ("_p")
            }
            else if (imageOrientation == "square") {
                maskToUse = maskToUse + ("_s")
            }

            let maskInfo: [Any] = [maskToUse, blurColor]
            NotificationCenter.default.post(name: "InsertMask", object: maskInfo)
        }
    }

    //-------------------------------------------------------------------------//
// MARK: - Memory Warning
    //-------------------------------------------------------------------------//
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}