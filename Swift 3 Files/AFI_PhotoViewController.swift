//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  AFIPhotoViewController.h
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 28/12/14.
//  Copyright (c) 2014 Nissi Vieira Miranda. All rights reserved.
//
import UIKit
class AFI_PhotoViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate {
    @IBOutlet weak var cameraFailAlertView: UIView!
    @IBOutlet weak var panoramicImageAlertView: UIView!
    @IBOutlet weak var narrowImageAlertView: UIView!

    private var popoverController: UIPopoverController?
    private var auxiliar: AFI_ClasseAuxiliar?
    private var userDefaults: UserDefaults?
    private var pushedViewController: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        auxiliar = AFI_ClasseAuxiliar.shared()
        userDefaults = UserDefaults.standard
        NotificationCenter.default.addObserver(self, selector: #selector(self.appDidEnterBackground), name: UIApplicationDidEnterBackgroundNotification, object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        auxiliar?.maxStageWidth = view.frame.size.width - 40.0
        auxiliar?.maxStageHeight = view.frame.size.height - (view.frame.size.height * 0.1042) - 40.0
    }

    //-------------------------------------------------------------------------//
// MARK: - Notifications
    //-------------------------------------------------------------------------//
    func appDidEnterBackground() {
        if pushedViewController {
            pushedViewController = false
            NotificationCenter.default.post(name: UIApplicationDidReceiveMemoryWarningNotification, object: nil)
        }
        //NSLog(@"appDidEnterBackground - Photo viewcontroller");
    }

    //-------------------------------------------------------------------------//
// MARK: - IBActions
    //-------------------------------------------------------------------------//
    @IBAction func cameraButtonTapped(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = .camera
            picker.allowsEditing = false
            picker.modalPresentationStyle = .fullScreen
            present(picker, animated: true, completion: { _ in })
        }
        else {
            cameraFailAlertView.isHidden = false
        }
    }

    @IBAction func photoAlbumButtonTapped(_ sender: UIButton) {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.allowsEditing = false
        popoverController = UIPopoverController(contentViewController: picker)
        popoverController?.delegate = self
        popoverController?.present(from: CGRect(x: CGFloat(sender.frame.size.width / 2), y: CGFloat(sender.frame.size.height / 2), width: CGFloat(1), height: CGFloat(1)), in: sender, permittedArrowDirections: .left, animated: true)
    }

    @IBAction func okButtonTapped(_ sender: UIButton) {
        cameraFailAlertView.isHidden = true
        panoramicImageAlertView.isHidden = true
        narrowImageAlertView.isHidden = true
    }

    //-------------------------------------------------------------------------//
// MARK: - UIImagePickerControllerDelegate
    //-------------------------------------------------------------------------//
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [AnyHashable: Any]) {
        let pickerImage: UIImage? = (info[UIImagePickerControllerOriginalImage] as? UIImage)
        picker.dismiss(animated: true, completion: { _ in })
        popoverController?.dismiss(animated: true)
        UIApplication.shared.statusBarHidden = true
        if (pickerImage?.size?.width > pickerImage?.size?.height) && (pickerImage?.size?.width / pickerImage?.size?.height > 1.91) {
            panoramicImageAlertView.isHidden = false
        }
        else if (pickerImage?.size?.width < pickerImage?.size?.height) && (pickerImage?.size?.height / pickerImage?.size?.width > 1.51) {
            narrowImageAlertView.isHidden = false
        }
        else {
            let imageData: Data? = .uiImageJPEGRepresentation()
            userDefaults?.set(imageData, forKey: "originalImage")
            userDefaults?.synchronize()
            pushedViewController = true
            performSegue(withIdentifier: "ToWorkTableVC", sender: nil)
        }

    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: { _ in })
        popoverController?.dismiss(animated: true)
        UIApplication.shared.statusBarHidden = true
    }

    //-------------------------------------------------------------------------//
// MARK: - Memory Warning
    //-------------------------------------------------------------------------//
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("didReceiveMemoryWarning")
    }
}

//
//  AFIPhotoViewController.m
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 28/12/14.
//  Copyright (c) 2014 Nissi Vieira Miranda. All rights reserved.
//