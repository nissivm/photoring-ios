//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  AFI_ScenarioCell.swift
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 20/03/15.
//  Copyright (c) 2015 Nissi Vieira Miranda. All rights reserved.
//
import UIKit
class AFI_ScenarioCell: UICollectionViewCell {
    @IBOutlet weak var scenarioImage: UIImageView!
    @IBOutlet weak var cellSelection: UIImageView!
}