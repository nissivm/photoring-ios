//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  AFI_ScenariosContainerView.swift
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 20/03/15.
//  Copyright (c) 2015 Nissi Vieira Miranda. All rights reserved.
//
import UIKit
class AFI_ScenariosContainerView: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var collectionView: UICollectionView!

    private var scenariosIcons = [Any]()
    private var idxPath: IndexPath?
    private var imageOrientation: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        scenariosIcons = ["icon_scenario_1", "icon_scenario_2", "icon_scenario_3", "icon_scenario_4", "icon_scenario_5", "icon_scenario_6", "icon_scenario_7", "icon_scenario_8", "icon_scenario_9", "icon_scenario_10", "icon_scenario_11", "icon_scenario_12", "icon_scenario_13", "icon_scenario_14", "icon_scenario_15", "icon_scenario_16", "icon_scenario_17", "icon_scenario_18", "icon_scenario_19", "icon_scenario_20", "icon_scenario_21", "icon_scenario_22", "icon_scenario_23", "icon_scenario_24", "icon_scenario_25", "icon_scenario_26", "icon_scenario_27", "icon_scenario_28", "icon_scenario_29", "icon_scenario_30", "icon_scenario_31", "icon_scenario_32", "icon_scenario_33", "icon_scenario_34", "icon_scenario_35", "icon_scenario_36", "icon_scenario_37", "icon_scenario_38", "icon_scenario_39", "icon_scenario_40", "icon_scenario_41", "icon_scenario_42", "icon_scenario_43", "icon_scenario_44", "icon_scenario_45", "icon_scenario_46", "icon_scenario_47", "icon_scenario_48", "icon_scenario_49", "icon_scenario_50"]
        idxPath = IndexPath(item: -1, section: 0)
        NotificationCenter.default.addObserver(self, selector: #selector(self.scenariosVCExposed), name: "ScenariosVCExposed", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeScenario), name: "InsertFrame", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeScenario), name: "InsertMask", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeScenario), name: "WorkTableWasCleaned", object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    //----------------------------------------------------------------------------------//
// MARK: - Notifications
    //----------------------------------------------------------------------------------//
    func scenariosVCExposed(_ notification: Notification) {
        imageOrientation = notification.object
        if idxPath?.item >= 0 {
            collectionView?.scrollToItem(at: idxPath, atScrollPosition: .centeredHorizontally, animated: true)
        }
    }

    func removeScenario() {
        if idxPath?.item >= 0 {
            idxPath = IndexPath(item: -1, section: 0)
            collectionView?.reloadData()
            collectionView?.scrollToItem(at: IndexPath(item: 0, section: 0), atScrollPosition: .centeredHorizontally, animated: true)
        }
    }

    //----------------------------------------------------------------------------------//
// MARK: - IBActions
    //----------------------------------------------------------------------------------//
    @IBAction func removeScenarioButtonTapped(_ sender: UIButton) {
        removeScenario()
        NotificationCenter.default.post(name: "RemoveScenario", object: nil)
    }

    @IBAction func closeButtonTapped(_ sender: UIButton) {
        NotificationCenter.default.post(name: "CloseContainerView", object: nil)
    }

    //----------------------------------------------------------------------------------//
// MARK: - UICollectionViewDataSource
    //----------------------------------------------------------------------------------//
    func numberOfSections(inCollectionView collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ view: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return scenariosIcons.count
    }

    func collectionView(_ cv: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: AFI_ScenarioCell? = (cv.dequeueReusableCell(withReuseIdentifier: "ScenarioCell", for: indexPath) as? AFI_ScenarioCell)
        let addr: String? = Bundle.main.path(forResource: (scenariosIcons[indexPath.item] as? String), ofType: "png")
        let image = UIImage(contentsOfFile: addr)
        cell?.scenarioImage?.image = image
        if idxPath?.item >= 0 {
            if idxPath?.compare(indexPath) == .orderedSame {
                cell?.cellSelection?.isHidden = false
            }
            else {
                cell?.cellSelection?.isHidden = true
            }
        }
        else {
            cell?.cellSelection?.isHidden = true
        }
        return cell!
    }

    //----------------------------------------------------------------------------------//
// MARK: - UICollectionViewDelegateFlowLayout
    //----------------------------------------------------------------------------------//
    func collectionView(_ cv: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSide: Float? = collectionView?.frame?.size?.width / 8
        return CGSize(width: CGFloat(cellSide), height: CGFloat(cellSide))
    }

    func collectionView(_ cv: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionat section: Int) -> CGFloat {
        return 0.0
    }

    func collectionView(_ cv: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionat section: Int) -> CGFloat {
        return 0.0
    }

    //-------------------------------------------------------------------------//
// MARK: - UICollectionViewDelegate
    //-------------------------------------------------------------------------//
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item != idxPath?.item {
            var cell: AFI_ScenarioCell?
            if idxPath?.item >= 0 {
                cell = (collectionView.cellForItem(at: idxPath) as? AFI_ScenarioCell)
                cell?.cellSelection?.isHidden = true
            }
            cell = (collectionView.cellForItem(at: indexPath) as? AFI_ScenarioCell)
            cell?.cellSelection?.isHidden = false
            idxPath = indexPath
            var scenarioToUse: String? = ((scenariosIcons[indexPath.item] as? String) as? NSString)?.substring(from: 5)
            if (imageOrientation == "portrait") {
                scenarioToUse = scenarioToUse! + ("_p")
            }
            else if (imageOrientation == "square") {
                scenarioToUse = scenarioToUse! + ("_s")
            }

            NotificationCenter.default.post(name: "InsertScenario", object: scenarioToUse)
        }
    }

    //-------------------------------------------------------------------------//
// MARK: - Memory Warning
    //-------------------------------------------------------------------------//
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}