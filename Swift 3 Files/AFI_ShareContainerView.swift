//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  AFI_ShareContainerView.swift
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 04/01/15.
//  Copyright (c) 2015 Nissi Vieira Miranda. All rights reserved.
//
import UIKit
class AFI_ShareContainerView: UIViewController {
    @IBOutlet weak var saveImageButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var successAlertView: UIView!
    @IBOutlet weak var errorAlertView: UIView!
    var library: ALAssetsLibrary?

    private var imageToShare: UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()
        library = ALAssetsLibrary()
        NotificationCenter.default.addObserver(self, selector: #selector(self.imageToShare), name: "ImageToShare", object: nil)
    }

    //-------------------------------------------------------------------------//
// MARK: - Notifications
    //-------------------------------------------------------------------------//
    func image(toShare notification: Notification) {
        imageToShare = notification.object
        saveImageButton.isHidden = false
        cancelButton.isHidden = false
    }

    //-------------------------------------------------------------------------//
// MARK: - IBActions
    //-------------------------------------------------------------------------//
    @IBAction func saveImageButtonTapped(_ sender: UIButton) {
        saveImageButton.isHidden = true
        cancelButton.isHidden = true
        let completion: ((_: URL, _: Error) -> Void)?? = {(_ assetURL: URL, _ error: Error?) -> Void in
                if error != nil {
                    errorAlertView.isHidden = false
                    let errorMessage: String? = "\(error?.description)"
                    print("Error when saving image: \(errorMessage)")
                }
                else {
                    successAlertView.isHidden = false
                }
            }
        let failure: ((_: Error) -> Void)?? = {(_ error: Error?) -> Void in
                if error != nil {
                    errorAlertView.isHidden = false
                    let errorMessage: String? = "\(error?.description)"
                    print("Error when saving image: \(errorMessage)")
                }
            }
        // Save image to custom photo album:
        library?.save(imageToShare, toAlbum: "Photoring", completion: completion, failure: failure)
    }

    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        imageToShare = nil
        NotificationCenter.default.post(name: "CloseContainerView", object: nil)
        saveImageButton.isHidden = true
        cancelButton.isHidden = true
    }

    @IBAction func okButtonTapped(_ sender: UIButton) {
        imageToShare = nil
        NotificationCenter.default.post(name: "CloseContainerView", object: nil)
        successAlertView.isHidden = true
        errorAlertView.isHidden = true
    }

    //-------------------------------------------------------------------------//
// MARK: - Memory Warning
    //-------------------------------------------------------------------------//
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}