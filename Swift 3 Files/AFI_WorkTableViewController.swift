//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  AFIDrawingViewController.h
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 18/12/14.
//  Copyright (c) 2014 App Magic. All rights reserved.
//
import UIKit
class AFI_WorkTableViewController: UIViewController {
    @IBOutlet weak var startOverButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var gpuImageView: UIView!
    @IBOutlet weak var toolbar: UIView!
    @IBOutlet weak var filtersContainerView: UIView!
    @IBOutlet weak var shareContainerView: UIView!
    @IBOutlet weak var framesContainerView: UIView!
    @IBOutlet weak var scenariosContainerView: UIView!
    @IBOutlet weak var masksContainerView: UIView!
    @IBOutlet weak var spinnerBackground: UIView!
    @IBOutlet weak var processingView: UIView!
    var imagemFiltrada: UIImage?

    private var auxiliar: AFI_ClasseAuxiliar?
    private var contViewShowing: UIView?
    private var userDefaults: UserDefaults?
    private var isRetina: Bool = false
    private var selectedFrame: String = ""
    private var selectedScenario: String = ""
    private var maskToUse: String = ""
    private var blurColor: UIColor?
    private var frameLayer: UIImageView?
    private var scenarioLayer: UIImageView?
    private var maskLayer: UIImageView?
    private var origImageHolder: UIImageView?

    override func viewDidLoad() {
        super.viewDidLoad()
        auxiliar = AFI_ClasseAuxiliar.shared()
        userDefaults = UserDefaults.standard
        imagemFiltrada = nil
            // Para contornar o fato de que nenhuma imagem é exibida inicialmente na gpuImageView:
        let imagemOriginal = UIImage(data: userDefaults?.object(forKey: "originalImage"))
        origImageHolder = UIImageView(frame: gpuImageView.frame)
        origImageHolder?.contentMode = .scaleAspectFit
        origImageHolder?.backgroundColor = UIColor.clear
        origImageHolder?.image = imagemOriginal
        view.insertSubview(origImageHolder, aboveSubview: gpuImageView)
        NotificationCenter.default.addObserver(self, selector: #selector(self.appDidEnterBackground), name: UIApplicationDidEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.appWillEnterForeground), name: UIApplicationWillEnterForegroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.closeContainerView), name: "CloseContainerView", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.insertFrame), name: "InsertFrame", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeFrame), name: "RemoveFrame", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.insertScenario), name: "InsertScenario", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeScenario), name: "RemoveScenario", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.insertMask), name: "InsertMask", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeMask), name: "RemoveMask", object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.lookupFilter), name: "LookupFilter", object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if UIScreen.main.responds(to: #selector(self.scale)) && UIScreen.main.scale() > 1 {
            isRetina = true
        }
        else {
            isRetina = false
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    //-------------------------------------------------------------------------//
// MARK: - Notifications
    //-------------------------------------------------------------------------//
    func appDidEnterBackground() {
        if frameLayer != nil {
            selectedScenario = nil
            maskToUse = nil
        }
        else if scenarioLayer != nil {
            selectedFrame = nil
            maskToUse = nil
        }
        else if maskLayer != nil {
            selectedScenario = nil
            selectedFrame = nil
        }
        else {
            selectedScenario = nil
            selectedFrame = nil
            maskToUse = nil
        }

        cleanWorkTable()
        if origImageHolder != nil {
            origImageHolder?.isHidden = false
        }
        else {
            gpuImageView.isHidden = false
        }
        //NSLog(@"appDidEnterBackground - Work table viewcontroller");
    }

    func appWillEnterForeground() {
        if selectedFrame != "" {
            NotificationCenter.default.post(name: "InsertFrame", object: selectedFrame)
        }
        else if selectedScenario != "" {
            NotificationCenter.default.post(name: "InsertScenario", object: selectedScenario)
        }
        else if maskToUse != "" {
            let maskInfo: [Any] = [maskToUse, blurColor]
            NotificationCenter.default.post(name: "InsertMask", object: maskInfo)
        }

        //NSLog(@"appWillEnterForeground");
    }

    func closeContainerView() {
        contViewShowing?.isHidden = true
        if toolbar.isHidden {
            toolbar.isHidden = false
        }
    }

    // Frames related notifications:
    func insertFrame(_ notification: Notification) {
        selectedFrame = notification.object
        cleanWorkTable()
        var frameToUse: CGRect = findOutFrameToUse_frameInserting()
        frameLayer = UIImageView(frame: frameToUse)
        frameLayer?.contentMode = .scaleAspectFit
        frameLayer?.backgroundColor = UIColor.clear
        frameToUse = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(frameToUse.size.width), height: CGFloat(frameToUse.size.height))
            // Image Layer:
        var backImage: UIImage?
        if imagemFiltrada != nil {
            backImage = imagemFiltrada
        }
        else {
            backImage = UIImage(data: userDefaults?.object(forKey: "originalImage"))
        }
        let layer_01 = UIImageView(frame: frameToUse)
        layer_01.contentMode = .scaleAspectFit
        layer_01.backgroundColor = UIColor.clear
        layer_01.image = auxiliar?.getImageUnderProportion(backImage)
            // Frame Layer:
        let frameAddr: String? = Bundle.main.path(forResource: selectedFrame, ofType: "png")
        var frameImage = UIImage(contentsOfFile: frameAddr)
        if isRetina == false {
            frameImage = auxiliar?.createNonRetinaImage(frameImage)
        }
        let layer_02 = UIImageView(frame: frameToUse)
        layer_02.contentMode = .scaleAspectFit
        layer_02.backgroundColor = UIColor.clear
        layer_02.image = frameImage
            // Container:
        let container = UIView(frame: frameToUse)
        container.backgroundColor = UIColor.clear
        container.addSubview(layer_01)
        container.addSubview(layer_02)
        frameLayer?.image = genImage(from: container)
        view.insertSubview(frameLayer, aboveSubview: gpuImageView)
        checkShareStartOverButtonsEnability()
    }

    func removeFrame() {
        if frameLayer != nil {
            frameLayer?.removeFromSuperview()
            frameLayer = nil
            if origImageHolder != nil {
                origImageHolder?.isHidden = false
            }
            else {
                gpuImageView.isHidden = false
            }
            checkShareStartOverButtonsEnability()
        }
    }

    //Scenarios related notifications:
    func insertScenario(_ notification: Notification) {
        selectedScenario = notification.object
        cleanWorkTable()
        var frameToUse: CGRect = findOutFrameToUse()
        scenarioLayer = UIImageView(frame: frameToUse)
        scenarioLayer?.contentMode = .scaleAspectFit
        scenarioLayer?.backgroundColor = UIColor.clear
        frameToUse = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(frameToUse.size.width), height: CGFloat(frameToUse.size.height))
            // Image Layer:
        var backImage: UIImage?
        if imagemFiltrada != nil {
            backImage = imagemFiltrada
        }
        else {
            backImage = UIImage(data: userDefaults?.object(forKey: "originalImage"))
        }
        let layer_01 = UIImageView(frame: frameToUse)
        layer_01.contentMode = .scaleAspectFit
        layer_01.backgroundColor = UIColor.clear
        layer_01.image = auxiliar?.getImageUnderProportion(backImage)
            // Scenario Layer:
        let scenarioAddr: String? = Bundle.main.path(forResource: selectedScenario, ofType: "png")
        var scenarioImage = UIImage(contentsOfFile: scenarioAddr)
        if isRetina == false {
            scenarioImage = auxiliar?.createNonRetinaImage(scenarioImage)
        }
        let layer_02 = UIImageView(frame: frameToUse)
        layer_02.contentMode = .scaleAspectFit
        layer_02.backgroundColor = UIColor.clear
        layer_02.image = scenarioImage
            // Container:
        let container = UIView(frame: frameToUse)
        container.backgroundColor = UIColor.clear
        container.addSubview(layer_01)
        container.addSubview(layer_02)
        scenarioLayer?.image = genImage(from: container)
        view.insertSubview(scenarioLayer, aboveSubview: gpuImageView)
        checkShareStartOverButtonsEnability()
    }

    func removeScenario() {
        if scenarioLayer != nil {
            scenarioLayer?.removeFromSuperview()
            scenarioLayer = nil
            if origImageHolder != nil {
                origImageHolder?.isHidden = false
            }
            else {
                gpuImageView.isHidden = false
            }
            checkShareStartOverButtonsEnability()
        }
    }

    //Masks related notifications:
    func insertMask(_ notification: Notification) {
        spinnerBackground.isHidden = false
        processingView.isHidden = false
        let maskInfo: [Any]? = notification.object
        maskToUse = (maskInfo?[0] as? String)
        blurColor = (maskInfo?[1] as? UIColor)
        var frameToUse: CGRect = findOutFrameToUse()
        frameToUse = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(frameToUse.size.width), height: CGFloat(frameToUse.size.height))
            // Image Layer:
        var imageToMask: UIImage?
        if imagemFiltrada != nil {
            imageToMask = imagemFiltrada
        }
        else {
            imageToMask = UIImage(data: userDefaults?.object(forKey: "originalImage"))
        }
        let layer_01 = UIImageView(frame: frameToUse)
        layer_01.contentMode = .scaleAspectFit
        layer_01.backgroundColor = UIColor.clear
        layer_01.image = auxiliar?.getImageUnderProportion(imageToMask)
            // Blurred view layer:
        let layer_02 = FXBlurView(frame: frameToUse)
        layer_02.`dynamic` = false
            // Color layer:
        var layer_03: UIView?
        if blurColor?.isEqual(UIColor.clear) == false {
            layer_03 = UIView(frame: frameToUse)
            layer_03?.backgroundColor = blurColor
            layer_03?.alpha = 0.5
        }
            // Masked image layer:
        let maskAddr: String? = Bundle.main.path(forResource: maskToUse, ofType: "png")
        var maskImage = UIImage(contentsOfFile: maskAddr)
        if isRetina == false {
            maskImage = auxiliar?.createNonRetinaImage(maskImage)
        }
        let layer_04 = UIImageView(frame: frameToUse)
        layer_04.contentMode = .scaleAspectFit
        layer_04.backgroundColor = UIColor.clear
        var maskedImage: UIImage?
        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
            maskedImage = self.auxiliar?.maskImage(layer_01.image, withMask: maskImage)
            DispatchQueue.main.async(execute: {() -> Void in
                layer_04.image = maskedImage
                    // Container:
                let container = UIView(frame: frameToUse)
                container.backgroundColor = UIColor.clear
                container.addSubview(layer_01)
                container.addSubview(layer_02)
                if layer_03 != nil {
                    container.addSubview(layer_03!)
                }
                container.addSubview(layer_04)
                self.cleanWorkTable()
                var frameToUse: CGRect = self.findOutFrameToUse()
                self.maskLayer = UIImageView(frame: frameToUse)
                self.maskLayer?.contentMode = .scaleAspectFit
                self.maskLayer?.backgroundColor = UIColor.clear
                self.maskLayer?.image = self.genImage(from: container)
                view.insertSubview(self.maskLayer, aboveSubview: self.gpuImageView)
                self.checkShareStartOverButtonsEnability()
                spinnerBackground.isHidden = true
                processingView.isHidden = true
            })
        })
    }

    func removeMask() {
        if maskLayer != nil {
            maskLayer?.removeFromSuperview()
            maskLayer = nil
            if origImageHolder != nil {
                origImageHolder?.isHidden = false
            }
            else {
                gpuImageView.isHidden = false
            }
            checkShareStartOverButtonsEnability()
        }
    }

    // Filters related notification:
    func lookupFilter(_ notification: Notification) {
        // Após a aplicação do 1o filtro, e mesmo após remover filtro,
        // a imagem passa a ser exibida em gpuImageView, e origImageHolder
        // passa a ser desnecessária:
        if origImageHolder != nil {
            origImageHolder?.removeFromSuperview()
            origImageHolder = nil
        }
        if notification.object == nil {
            imagemFiltrada = nil
        }
        else {
            let imagemOriginal = UIImage(data: userDefaults?.object(forKey: "originalImage"))
            let lookupFilter: GPUImageInput? = notification.object
            lookupFilter?.useNextFrameForImageCapture()
            imagemFiltrada = lookupFilter?.imageFromCurrentFramebuffer()
            imagemFiltrada = UIImage(cgImage: imagemFiltrada?.cgImage, scale: imagemOriginal?.scale, orientation: imagemOriginal?.imageOrientation)
        }
        if frameLayer != nil {
            NotificationCenter.default.post(name: "InsertFrame", object: selectedFrame)
        }
        else if scenarioLayer != nil {
            NotificationCenter.default.post(name: "InsertScenario", object: selectedScenario)
        }
        else if maskLayer != nil {
            let maskInfo: [Any] = [maskToUse, blurColor]
            NotificationCenter.default.post(name: "InsertMask", object: maskInfo)
        }

        checkShareStartOverButtonsEnability()
    }

    //-------------------------------------------------------------------------//
// MARK: - IBActions
    //-------------------------------------------------------------------------//
    @IBAction func backButtonTapped(_ sender: UIButton) {
        userDefaults?.removeObject(forKey: "originalImage")
        userDefaults?.synchronize()
        navigationController?.popViewController(animated: true)
    }

    @IBAction func startOverButtonTapped(_ sender: UIButton) {
        if startOverButton.isEnabled {
            cleanWorkTable()
            if origImageHolder != nil {
                origImageHolder?.isHidden = false
            }
            else {
                gpuImageView.isHidden = false
            }
            checkShareStartOverButtonsEnability()
            NotificationCenter.default.post(name: "WorkTableWasCleaned", object: nil)
        }
    }

    @IBAction func filtersButtonTapped(_ sender: UIButton) {
        toolbar.isHidden = true
        contViewShowing = filtersContainerView
        contViewShowing?.isHidden = false
        NotificationCenter.default.post(name: "FiltersVCExposed", object: nil)
    }

    @IBAction func masksButtonTapped(_ sender: UIButton) {
        toolbar.isHidden = true
        contViewShowing = masksContainerView
        contViewShowing?.isHidden = false
        let imageOrientation: String? = auxiliar?.getImageOrientation(UIImage(data: userDefaults?.object(forKey: "originalImage")!))
        NotificationCenter.default.post(name: "MasksVCExposed", object: imageOrientation)
    }

    @IBAction func scenariosButtonTapped(_ sender: UIButton) {
        toolbar.isHidden = true
        contViewShowing = scenariosContainerView
        contViewShowing?.isHidden = false
        let imageOrientation: String? = auxiliar?.getImageOrientation(UIImage(data: userDefaults?.object(forKey: "originalImage")!))
        NotificationCenter.default.post(name: "ScenariosVCExposed", object: imageOrientation)
    }

    @IBAction func framesButtonTapped(_ sender: UIButton) {
        toolbar.isHidden = true
        contViewShowing = framesContainerView
        contViewShowing?.isHidden = false
        let imageOrientation: String? = auxiliar?.getImageOrientation(UIImage(data: userDefaults?.object(forKey: "originalImage")!))
        NotificationCenter.default.post(name: "FramesVCExposed", object: imageOrientation)
    }

    @IBAction func shareButtonTapped(_ sender: UIButton) {
        if shareButton.isEnabled {
            generateAndPostImageToShare()
            toolbar.isHidden = true
            contViewShowing = shareContainerView
            contViewShowing?.isHidden = false
        }
    }

    //-------------------------------------------------------------------------//
// MARK: - Auxiliar Methods
    //-------------------------------------------------------------------------//
    func cleanWorkTable() {
        if origImageHolder != nil {
            origImageHolder?.isHidden = true
        }
        else {
            gpuImageView.isHidden = true
        }
        if scenarioLayer != nil {
            scenarioLayer?.removeFromSuperview()
            scenarioLayer = nil
        }
        if frameLayer != nil {
            frameLayer?.removeFromSuperview()
            frameLayer = nil
        }
        if maskLayer != nil {
            maskLayer?.removeFromSuperview()
            maskLayer = nil
        }
    }

    // Usado somente com scenarios e masks:
    func findOutFrameToUse() -> CGRect {
        let landWidth: Double = gpuImageView.frame.size.height * 1.52
        let landX: Double = (view.frame.size.width - landWidth) / 2
        let portWidth: Double = gpuImageView.frame.size.height / 1.35
        let portX: Double = (view.frame.size.width - portWidth) / 2
        let squareWidth: Double = gpuImageView.frame.size.height
        let squareX: Double = (view.frame.size.width - squareWidth) / 2
        let landscapeFrame = CGRect(x: CGFloat(landX), y: CGFloat(gpuImageView.frame.origin.y), width: CGFloat(landWidth), height: CGFloat(gpuImageView.frame.size.height))
        let portraitFrame = CGRect(x: CGFloat(portX), y: CGFloat(gpuImageView.frame.origin.y), width: CGFloat(portWidth), height: CGFloat(gpuImageView.frame.size.height))
        let squareFrame = CGRect(x: CGFloat(squareX), y: CGFloat(gpuImageView.frame.origin.y), width: CGFloat(squareWidth), height: CGFloat(gpuImageView.frame.size.height))
        let imageOrientation: String? = auxiliar?.getImageOrientation(UIImage(data: userDefaults?.object(forKey: "originalImage")!))
        var frameToUse: CGRect
        if (imageOrientation == "landscape") {
            frameToUse = landscapeFrame
        }
        else if (imageOrientation == "portrait") {
            frameToUse = portraitFrame
        }
        else {
            frameToUse = squareFrame
        }

        return frameToUse
    }

    // Usado com os frames:
    func findOutFrameToUse_frameInserting() -> CGRect {
        let portWidth: Double = gpuImageView.frame.size.width / 2
        let portX: Double = ((view.frame.size.width - portWidth) / 2)
        let squareX: Double = ((view.frame.size.width - gpuImageView.frame.size.height) / 2)
        let landscapeFrame: CGRect = gpuImageView.frame
        let portraitFrame = CGRect(x: CGFloat(portX), y: CGFloat(gpuImageView.frame.origin.y), width: CGFloat(portWidth), height: CGFloat(gpuImageView.frame.size.height))
        let squareFrame = CGRect(x: CGFloat(squareX), y: CGFloat(gpuImageView.frame.origin.y), width: CGFloat(gpuImageView.frame.size.height), height: CGFloat(gpuImageView.frame.size.height))
        let imageOrientation: String? = auxiliar?.getImageOrientation(UIImage(data: userDefaults?.object(forKey: "originalImage")!))
        var frameToUse: CGRect
        if (imageOrientation == "landscape") {
            frameToUse = landscapeFrame
        }
        else if (imageOrientation == "portrait") {
            frameToUse = portraitFrame
        }
        else {
            frameToUse = squareFrame
        }

        return frameToUse
    }

    func genImage(from view: UIView) -> UIImage {
        var image: UIImage?
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }

    func checkShareStartOverButtonsEnability() {
        if frameLayer || scenarioLayer || maskLayer {
            startOverButton.isEnabled = true
            shareButton.isEnabled = true
        }
        else {
            startOverButton.isEnabled = false
            shareButton.isEnabled = false
        }
        if imagemFiltrada != nil {
            shareButton.isEnabled = true
        }
    }

    func generateAndPostImageToShare() {
        if frameLayer != nil {
            let imagem: UIImage? = genImage(from: frameLayer)
            NotificationCenter.default.post(name: "ImageToShare", object: imagem)
        }
        else if scenarioLayer != nil {
            let imagem: UIImage? = genImage(from: scenarioLayer)
            NotificationCenter.default.post(name: "ImageToShare", object: imagem)
        }
        else if maskLayer != nil {
            let imagem: UIImage? = genImage(from: maskLayer)
            NotificationCenter.default.post(name: "ImageToShare", object: imagem)
        }
        else {
            let rect = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(imagemFiltrada?.size?.width), height: CGFloat(imagemFiltrada?.size?.height))
            let imageView = UIImageView(frame: rect)
            imageView.contentMode = .scaleAspectFit
            imageView.backgroundColor = UIColor.clear
            imageView.image = imagemFiltrada
            let imagem: UIImage? = genImage(from: imageView)
            NotificationCenter.default.post(name: "ImageToShare", object: imagem)
        }

    }

    //-------------------------------------------------------------------------//
// MARK: - Memory Warning
    //-------------------------------------------------------------------------//
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//
//  AFIDrawingViewController.m
//  Artistic Finger Images
//
//  Created by Nissi Vieira Miranda on 18/12/14.
//  Copyright (c) 2014 App Magic. All rights reserved.
//