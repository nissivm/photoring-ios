//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  ALAssetsLibrary category to handle a custom photo album
//
//  Created by Marin Todorov on 10/26/11.
//  Copyright (c) 2011 Marin Todorov. All rights reserved.
//
import Foundation
import UIKit
import AssetsLibrary
extension ALAssetsLibrary {
    /*! Write the image data to the assets library (camera roll).
     *
     * \param image The target image to be saved
     * \param albumName Custom album name
     * \param completion Block to be executed when succeed to write the image data to the assets library (camera roll)
     * \param failure Block to be executed when failed to add the asset to the custom photo album
     */

// MARK: - Private Method

    func save(_ image: UIImage, toAlbum albumName: String, completion: ALAssetsLibraryWriteImageCompletionBlock, failure: ALAssetsLibraryAccessFailureBlock) {
        writeImage(toSavedPhotosAlbum: image.cgImage, orientation: (image.imageOrientation as? ALAssetOrientation), completionBlock: _resultBlockOfAdding(toAlbum: albumName, completion: completion, failure: failure))
    }

    /*! write the video to the assets library (camera roll).
     *
     * \param videoUrl The target video to be saved
     * \param albumName Custom album name
     * \param completion Block to be executed when succeed to write the image data to the assets library (camera roll)
     * \param failure block to be executed when failed to add the asset to the custom photo album
     */
    func saveVideo(_ videoUrl: URL, toAlbum albumName: String, completion: ALAssetsLibraryWriteImageCompletionBlock, failure: ALAssetsLibraryAccessFailureBlock) {
        writeVideoAtPath(toSavedPhotosAlbum: videoUrl, completionBlock: _resultBlockOfAdding(toAlbum: albumName, completion: completion, failure: failure))
    }

    /*! Write the image data with meta data to the assets library (camera roll).
     * 
     * \param imageData The image data to be saved
     * \param albumName Custom album name
     * \param metadata Meta data for image
     * \param completion Block to be executed when succeed to write the image data
     * \param failure block to be executed when failed to add the asset to the custom photo album
     */
    func saveImageData(_ imageData: Data, toAlbum albumName: String, metadata: [AnyHashable: Any], completion: ALAssetsLibraryWriteImageCompletionBlock, failure: ALAssetsLibraryAccessFailureBlock) {
        writeImageData(toSavedPhotosAlbum: imageData, metadata: metadata, completionBlock: _resultBlockOfAdding(toAlbum: albumName, completion: completion, failure: failure))
    }

    /*! Write the asset to the assets library (camera roll).
     *
     * \param assetURL The asset URL
     * \param albumName Custom album name
     * \param failure Block to be executed when failed to add the asset to the custom photo album
     */
    func addAssetURL(_ assetURL: URL, toAlbum albumName: String, completion: ALAssetsLibraryWriteImageCompletionBlock, failure: ALAssetsLibraryAccessFailureBlock) {
        var albumWasFound: Bool = false
            // Signature for the block executed when a match is found during enumeration using
            //   |-enumerateGroupsWithTypes:usingBlock:failureBlock:|.
            //
            // |group|: The current asset group in the enumeration.
            // |stop| : A pointer to a boolean value; set the value to YES to stop enumeration.
            //
        var enumerationBlock: ALAssetsLibraryGroupsEnumerationResultsBlock
        enumerationBlock = {(_ group: ALAssetsGroup, _ stop: Bool) -> Void in
            // Compare the names of the albums
            if albumName.compare(group.value(forProperty: ALAssetsGroupPropertyName)) == .orderedSame {
                // Target album is found
                albumWasFound = true
                    // Get a hold of the photo's asset instance
                    // If the user denies access to the application, or if no application is allowed to
                    //   access the data, the failure block is called.
                let assetForURLResultBlock: ALAssetsLibraryAssetForURLResultBlock = self._assetForURLResultBlock(with: group, assetURL: assetURL, completion: completion, failure: failure)
                self.asset(forURL: assetURL, resultBlock: assetForURLResultBlock, failureBlock: failure)
                // Album was found, bail out of the method
                stop = true
            }
            if group == nil && albumWasFound == false {
                    // Photo albums are over, target album does not exist, thus create it
                    // Since you use the assets library inside the block,
                    //   ARC will complain on compile time that there’s a retain cycle.
                    //   When you have this – you just make a weak copy of your object.
                let weakSelf: ALAssetsLibrary? = self
                let addPhotoToLibraryBlock: ((_ group: ALAssetsGroup) -> Void)?? = {(_ group: ALAssetsGroup) -> Void in
                            // Get the photo's instance
                            //   add the photo to the newly created album
                        let assetForURLResultBlock: ALAssetsLibraryAssetForURLResultBlock? = weakSelf?._assetForURLResultBlock(with: group, assetURL: assetURL, completion: completion, failure: failure)
                        weakSelf?.asset(forURL: assetURL, resultBlock: assetForURLResultBlock, failureBlock: failure)
                    }
                // If iOS version is lower than 5.0, throw a warning message
                if !self.responds(to: Selector("addAssetsGroupAlbumWithName:resultBlock:failureBlock:")) {
                    print("\(#function): WARNING: |-addAssetsGroupAlbumWithName:resultBlock:failureBlock:| " + 
"              only available on iOS 5.0 or later. Asset cannot be saved to album.")
                }
                else {
                        // Different code for iOS 7 and 8
                        // See: http://stackoverflow.com/questions/26003211/assetslibrary-framework-broken-on-ios-8
                        // See: http://stackoverflow.com/questions/8867496/get-last-image-from-photos-app/8872425#8872425
                        // PHPhotoLibrary_class will only be non-nil on iOS 8.0 or later.
                    let PHPhotoLibrary_class: AnyClass = NSClassFromString("PHPhotoLibrary")
                    if PHPhotoLibrary_class {
                            // dynamic runtime code for code chunk listed above
//clang diagnostic push
//clang diagnostic ignored "-Warc-performSelector-leaks"
                        let sharedPhotoLibrary: Any? = PHPhotoLibrary_class.perform(NSSelectorFromString("sharedPhotoLibrary"))
//clang diagnostic pop
                        let shouldInvokeSuccessBlockInMainThread: Bool = (Thread.current == Thread.main)
                        var performChanges: Selector
                        if shouldInvokeSuccessBlockInMainThread {
                            // Synchronously runs a block that requests changes to be performed in the Photos library
                            performChanges = NSSelectorFromString("performChangesAndWait:error:")
                        }
                        else {
                            // Asynchronously runs a block that requests changes to be performed in the Photos library
                            performChanges = NSSelectorFromString("performChanges:completionHandler:")
                        }
                        let methodSignature: NSMethodSignature? = sharedPhotoLibrary?.methodSignature(for: performChanges)
                        let invocation = NSInvocation(methodSignature)
                        invocation.target = sharedPhotoLibrary
                        invocation.selector = performChanges
                            // Set the |changeBlock| for |-performChangesAndWait:error:| or |-performChanges:completionHandler:|.
                        var changeBlock: (() -> Void)? = {() -> Void in
                                let PHAssetCollectionChangeRequest_class: AnyClass = NSClassFromString("PHAssetCollectionChangeRequest")
                                let creationRequestForAssetCollectionWithTitle: Selector = NSSelectorFromString("creationRequestForAssetCollectionWithTitle:")
//clang diagnostic push
//clang diagnostic ignored "-Warc-performSelector-leaks"
                                PHAssetCollectionChangeRequest_class.perform(creationRequestForAssetCollectionWithTitle, with: albumName)
//clang diagnostic pop
                            }
                        invocation.setArgument(changeBlock, atIndex: 2)
                            // Block to be invoked after created album succeed.
                        let blockToEnumerateGroups: (() -> Void)? = {() -> Void in
                                self.enumerateGroups(withTypes: ALAssetsGroupAlbum, usingBlock: {(_ group: ALAssetsGroup, _ stop: Bool) -> Void in
                                    if group {
                                        let name: String = group.value(forProperty: ALAssetsGroupPropertyName)
                                        if (albumName == name) {
                                            addPhotoToLibraryBlock(group)
                                        }
                                    }
                                }, failureBlock: failure)
                            }
                        // Setup invocation to perfom selector |-performChangesAndWait:error:| in main thread.
                        if shouldInvokeSuccessBlockInMainThread {
                                // Set error point for |-performChangesAndWait:error:|.
                            var error: Error? = nil
                            invocation.setArgument(error, atIndex: 3)
                            invocation.invoke()
                                // Get return value of |-performChangesAndWait:error:|.
                            var createAlbumSucceed: Bool
                            invocation.getReturnValue(createAlbumSucceed)
                            if createAlbumSucceed {
                                blockToEnumerateGroups()
                            }
                            else {
                                if error != nil {
                                    print("\(#function): Error creating album (\(albumName)) :  \(error?.localizedDescription)")
                                }
                            }
                        }
                        else {
                            var completionHandler: ((_ success: Bool, _ error: Error?) -> Void)?? = {(_ success: Bool, _ error: Error?) -> Void in
                                    if success {
                                        blockToEnumerateGroups()
                                    }
                                    else {
                                        if error != nil {
                                            print("\(#function): Error creating album (\(albumName)) : \(error?.localizedDescription)")
                                        }
                                    }
                                }
                            // Set the |completionHandler| for |-performChanges:completionHandler:|.
                            invocation.setArgument(completionHandler, atIndex: 3)
                            invocation.invoke()
                        }
                    }
                    else {
                        // code that always creates an album on iOS 7.x.x but fails
                        // in certain situations such as if album has been deleted
                        // previously on iOS 8.x.
                        self.addAssetsGroupAlbum(withName: albumName, resultBlock: addPhotoToLibraryBlock, failureBlock: failure)
                    }
                }
                // Should be the last iteration anyway, but just in case
                stop = true
            }
        }
        // Search all photo albums in the library
        enumerateGroups(withTypes: ALAssetsGroupAlbum, usingBlock: enumerationBlock, failureBlock: failure)
    }

    /*! Loads assets from the assets group (album)
     *
     * \param albumName Custom album name
     * \param completion Block to be executed when succeed or failed to load images from target album
     */
    func loadImages(fromAlbum albumName: String, completion: @escaping (_: [Any], _: Error) -> Void) {
        let block: ALAssetsLibraryGroupsEnumerationResultsBlock? = {(_ group: ALAssetsGroup, _ stop: Bool) -> Void in
                // Checking if library exists
                if group == nil {
                    stop = true
                    return
                }
                // If we have found library with given title we enumerate it
                if albumName.compare(group.value(forProperty: ALAssetsGroupPropertyName)) == .orderedSame {
                    var images = [Any]()
                    group.enumerateAssets(usingBlock: {(_ result: ALAsset, _ index: Int, _ stop: Bool) -> Void in
                        // Checking if group isn't empty
                        if !result {
                            return
                        }
                            // Getting the image from the asset
                        let orientation: UIImageOrientation? = (CInt(result.value(forProperty: "ALAssetPropertyOrientation")) as? UIImageOrientation)
                        let image = UIImage(cgImage: result.defaultRepresentation().fullScreenImage(), scale: 1.0, orientation: orientation)
                        // Saving this image to the array
                        images.append(image)
                    })
                    // Execute the |completion| block
                    if completion {
                        completion(images, nil)
                    }
                    // Album was found, bail out of the method
                    stop = true
                }
            }
        let failureBlock: ALAssetsLibraryAccessFailureBlock? = {(_ error: Error?) -> Void in
                print("\(#function): \(error?.localizedDescription)")
                if completion {
                    completion(nil, error)
                }
            }
        enumerateGroups(withTypes: ALAssetsGroupAll, usingBlock: block, failureBlock: failureBlock)
    }

    func _resultBlockOfAdding(toAlbum albumName: String, completion: ALAssetsLibraryWriteImageCompletionBlock, failure: ALAssetsLibraryAccessFailureBlock) -> ALAssetsLibraryWriteImageCompletionBlock {
        return {(_ assetURL: URL, _ error: Error?) -> Void in
            // Run the completion block for writing image to saved
            //   photos album
            //if (completion) completion(assetURL, error);
            // If an error occured, do not try to add the asset to
            //   the custom photo album
            if error != nil {
                if failure {
                    failure(error)
                }
                return
            }
            // Add the asset to the custom photo album
            self.addAssetURL(assetURL, toAlbum: albumName, completion: completion, failure: failure)
        }
    }

    func _assetForURLResultBlock(with group: ALAssetsGroup, assetURL: URL, completion: ALAssetsLibraryWriteImageCompletionBlock, failure: ALAssetsLibraryAccessFailureBlock) -> ALAssetsLibraryAssetForURLResultBlock {
        return {(_ asset: ALAsset) -> Void in
            // Add photo to the target album
            if group.addAsset(asset) {
                // Run the completion block if the asset was added successfully
                if completion {
                    completion(assetURL, nil)
                }
            }
            else {
                let message: String = "ALAssetsGroup failed to add asset: \(asset)."
                failure(Error(domain: "LIB_ALAssetsLibrary_CustomPhotoAlbum", code: 0, userInfo: [NSLocalizedDescriptionKey: message]))
            }
        }
    }

// MARK: - Public Method
}

#if !__has_feature(objc_arc)
This class requires automatic reference counting (ARC).
#endif
extension ALAssetsLibrary {
    /*! A block wraper to be executed after asset adding process done. (Private)
     *
     * \param albumName Custom album name
     * \param completion Block to be executed when succeed to add the asset to the assets library (camera roll)
     * \param failure Block to be executed when failed to add the asset to the custom photo album
     */

    func _resultBlockOfAdding(toAlbum albumName: String, completion: ALAssetsLibraryWriteImageCompletionBlock, failure: ALAssetsLibraryAccessFailureBlock) -> ALAssetsLibraryWriteImageCompletionBlock {
    }

    /*! A block wraper to be executed after |-assetForURL:resultBlock:failureBlock:| succeed.
     *  Generally, this block will be excused when user confirmed the application's access
     *    to the library.
     *
     * \param group A group to be used to add photo to the target album
     * \param assetURL The URL for the target asset
     * \param completion Block to be executed when succeed to add the asset to the assets library (camera roll)
     * \param failure Block to be executed when failed to add the asset to the custom photo album
     *
     * \return An ALAssetsLibraryAssetForURLResultBlock type block
     */
    func _assetForURLResultBlock(with group: ALAssetsGroup, assetURL: URL, completion: ALAssetsLibraryWriteImageCompletionBlock, failure: ALAssetsLibraryAccessFailureBlock) -> ALAssetsLibraryAssetForURLResultBlock {
    }
}