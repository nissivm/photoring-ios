//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  FXBlurView.swift
//
//  Version 1.6.3
//
//  Created by Nick Lockwood on 25/08/2013.
//  Copyright (c) 2013 Charcoal Design
//
//  Distributed under the permissive zlib License
//  Get the latest version from here:
//
//  https://github.com/nicklockwood/FXBlurView
//
//  This software is provided 'as-is', without any express or implied
//  warranty.  In no event will the authors be held liable for any damages
//  arising from the use of this software.
//
//  Permission is granted to anyone to use this software for any purpose,
//  including commercial applications, and to alter it and redistribute it
//  freely, subject to the following restrictions:
//
//  1. The origin of this software must not be misrepresented; you must not
//  claim that you wrote the original software. If you use this software
//  in a product, an acknowledgment in the product documentation would be
//  appreciated but is not required.
//
//  2. Altered source versions must be plainly marked as such, and must not be
//  misrepresented as being the original software.
//
//  3. This notice may not be removed or altered from any source distribution.
//
import UIKit
import QuartzCore
import Accelerate
//GCC diagnostic push
//GCC diagnostic ignored "-Wobjc-missing-property-synthesis"
import Availability
//#undef weak_ref
#if __has_feature(objc_arc) && __has_feature(objc_arc_weak)
let weak_ref = `weak`
#else
let weak_ref = unsafe_unretained
#endif
extension UIImage {
    func blurredImage(withRadius radius: CGFloat, iterations: Int, tintColor: UIColor) -> UIImage {
        //image must be nonzero size
        if floorf(size.width) * floorf(size.height) <= 0.0 {

        }
            //boxsize must be an odd integer
        let boxSize = UInt32(radius * scale)
        if boxSize % 2 == 0 {
            boxSize += 1
        }
            //create image buffers
        var imageRef: CGImageRef? = cgImage
        var buffer1: vImage_Buffer
        var buffer2: vImage_Buffer
        buffer1.width = buffer2.width = CGImageGetWidth(imageRef)
        buffer1.height = buffer2.height = CGImageGetHeight(imageRef)
        buffer1.rowBytes = buffer2.rowBytes = CGImageGetBytesPerRow(imageRef)
        let bytes: size_t = buffer1.rowBytes * buffer1.height
        buffer1.data = malloc(bytes)
        buffer2.data = malloc(bytes)
            //create temp buffer
        let tempBuffer = malloc((vImageBoxConvolve_ARGB8888(buffer1, buffer2, nil, 0, 0, boxSize, boxSize, nil, kvImageEdgeExtend + kvImageGetTempBufferSize) as? size_t))
            //copy image data
        let dataSource: CFDataRef = CGImageGetDataProvider(imageRef).data()
        memcpy(buffer1.data, CFDataGetBytePtr(dataSource), bytes)
        for i in 0..<iterations {
            //perform blur
            vImageBoxConvolve_ARGB8888(buffer1, buffer2, tempBuffer, 0, 0, boxSize, boxSize, nil, kvImageEdgeExtend)
                //swap buffers
            let temp = buffer1.data
            buffer1.data = buffer2.data
            buffer2.data = temp
        }
        //free buffers
        free(buffer2.data)
        free(tempBuffer)
            //create image context from buffer
        let ctx = CGContext(data: buffer1.data, width: buffer1.width, height: buffer1.height, bitsPerComponent: 8, bytesPerRow: buffer1.rowBytes, space: CGImageGetColorSpace(imageRef), bitmapInfo: CGImageGetBitmapInfo(imageRef))
        //apply tint
        if tintColor && CGColorGetAlpha(tintColor.cgColor) > 0.0 {
            ctx.setFillColor(tintColor.withAlphaComponent(0.25).cgColor)
            CGContextSetBlendMode(ctx, kCGBlendModePlusLighter)
            ctx.fill(CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(buffer1.width), height: CGFloat(buffer1.height)))
        }
        //create image from context
        imageRef = ctx.makeImage()
        let image = UIImage(cgImage: imageRef, scale: scale, orientation: imageOrientation)
        CGImageRelease(imageRef)
        CGContextRelease(ctx)
        free(buffer1.data)
        return image
    }
}

class FXBlurView: UIView {
    var isBlurEnabled: Bool = false
    var is dynamic : Bool = false
    var iterations: Int = 0
    var updateInterval = TimeInterval()
    var blurRadius: CGFloat = 0.0
    var tintColor: UIColor?
    @IBOutlet var underlyingView: UIView!

    var isIterationsSet: Bool = false
    var isBlurRadiusSet: Bool = false
    var isDynamicSet: Bool = false
    var isBlurEnabledSet: Bool = false
    var lastUpdate: Date?

    class func setBlurEnabled(_ blurEnabled: Bool) {
        FXBlurScheduler.sharedInstance().isBlurEnabled = blurEnabled
    }

    class func setUpdatesEnabled() {
        FXBlurScheduler.sharedInstance().setUpdatesEnabled()
    }

    class func setUpdatesDisabled() {
        FXBlurScheduler.sharedInstance().setUpdatesDisabled()
    }

    func updateAsynchronously(_ async: Bool, completion: @escaping () -> Void) {
    }

    private func snapshotOfUnderlyingView() -> UIImage {
    }

    private func shouldUpdate() -> Bool {
    }

    class func layerClass() -> AnyClass {
        return FXBlurLayer.self
    }

    func setUp() {
        if !isIterationsSet {
            iterations = 3
        }
        if !isBlurRadiusSet {
            blurLayer().blurRadius = 40
        }
        if !isDynamicSet {
            `dynamic` = true
        }
        if !isBlurEnabledSet {
            isBlurEnabled = true
        }
        updateInterval = updateInterval
        layer.magnificationFilter = "linear"
            // kCAFilterLinear
        var numberOfMethods: UInt
        let methods: Method? = class_copyMethodList(UIView.self, numberOfMethods)
        for i in 0..<numberOfMethods {
            let method: Method = methods[i]
            let selector: Selector = method_getName(method)
            if selector == #selector(self.tintColor) {
                tintColor =
            }
        }
        (id, SEL)
    }
}

//GCC diagnostic pop
import ObjectiveC
//GCC diagnostic ignored "-Wobjc-missing-property-synthesis"
//GCC diagnostic ignored "-Wdirect-ivar-access"
//GCC diagnostic ignored "-Wgnu"
import Availability
#if !__has_feature(objc_arc)
This class requires automatic reference counting
#endif

class FXBlurScheduler: NSObject {
    var views = [Any]()
    var viewIndex: Int = 0
    var updatesEnabled: Int = 0
    var isBlurEnabled: Bool = false
    var isUpdating: Bool = false

    class func sharedInstance() -> FXBlurScheduler {
        var sharedInstance: FXBlurScheduler? = nil
        if sharedInstance == nil {
            sharedInstance = FXBlurScheduler()
        }
        return sharedInstance!
    }

    override init() {
        if (super.init()) {
            updatesEnabled = 1
            isBlurEnabled = true
            views = [Any]()
        }
    }

    func setBlurEnabled(_ blurEnabled: Bool) {
        isBlurEnabled = blurEnabled
        if blurEnabled {
            for view: FXBlurView in views {
                view.setNeedsDisplay()
            }
            updateAsynchronously()
        }
    }

    func setUpdatesEnabled() {
        updatesEnabled += 1
        updateAsynchronously()
    }

    func setUpdatesDisabled() {
        updatesEnabled -= 1
    }

    func add(_ view: FXBlurView) {
        if !views.contains(view) {
            views.append(view)
            updateAsynchronously()
        }
    }

    func remove(_ view: FXBlurView) {
        let index: Int = (views as NSArray).index(of: view)
        if index != NSNotFound {
            if index <= viewIndex {
                viewIndex -= 1
            }
            views.remove(at: index)
        }
    }

    func updateAsynchronously() {
        if isBlurEnabled && !isUpdating && updatesEnabled > 0 && views.count {
            var timeUntilNextUpdate: TimeInterval = 1.0 / 60
            //loop through until we find a view that's ready to be drawn
            viewIndex = viewIndex % views.count
            for i in viewIndex..<views.count {
                let view: FXBlurView? = views[i]
                if view?.is`dynamic` && !view?.isHidden && view?.window && view?.shouldUpdate() {
                    let nextUpdate: TimeInterval? = view?.lastUpdate?.timeIntervalSinceNow + view?.updateInterval
                    if !view?.lastUpdate || nextUpdate <= 0 {
                        isUpdating = true
                        view?.updateAsynchronously(true, completion: {() -> Void in
                            //render next view
                            isSelf.isUpdating = false
                            self.viewIndex = i + 1
                            self.updateAsynchronously()
                        })
                        return
                    }
                    else {
                        timeUntilNextUpdate = min(timeUntilNextUpdate, nextUpdate)
                    }
                }
            }
            //try again, delaying until the time when the next view needs an update.
            viewIndex = 0
            performSelector(#selector(self.updateAsynchronously), withObject: nil, afterDelay: timeUntilNextUpdate, inModes: [NSDefaultRunLoopMode, UITrackingRunLoopMode])
        }
    }
}

class FXBlurLayer: CALayer {
    var blurRadius: CGFloat = 0.0

    class func needsDisplay(forKey key: String) -> Bool {
        if ["blurRadius", "bounds", "position"].contains(key) {
            return true
        }
        return super.needsDisplay(forKey: key)
    }
}

free(methods)
override init(frame: CGRect) {
    if (super.init(frame: frame)) {
        setUp()
        clipsToBounds = true
    }
}

required init?(coder aDecoder: NSCoder) {
    if (super.init(coder: aDecoder)) {
        setUp()
    }
}

deinit {
    NotificationCenter.default.removeObserver(self)
}

func setIterations(_ iterations: Int) {
    isIterationsSet = true
    self.iterations = iterations
    setNeedsDisplay()
}

func setBlurRadius(_ blurRadius: CGFloat) {
    isBlurRadiusSet = true
    blurLayer().blurRadius = blurRadius
}

func blurRadius() -> CGFloat {
    return blurLayer().blurRadius()
}

func setBlurEnabled(_ blurEnabled: Bool) {
    isBlurEnabledSet = true
    if isBlurEnabled != blurEnabled {
        isBlurEnabled = blurEnabled
        schedule()
        if isBlurEnabled {
            setNeedsDisplay()
        }
    }
}

func setDynamic(_ dynamic : Bool) {
    isDynamicSet = true
    if self.dynamic != `dynamic` {
        self.dynamic = `dynamic`
        schedule()
        if !`dynamic` {
            setNeedsDisplay()
        }
    }
}

func underlyingView() -> UIView? {
    return underlyingView ?? superview!
}

func underlyingLayer() -> CALayer {
    return underlyingView().layer
}

func blurLayer() -> FXBlurLayer {
    return (layer as? FXBlurLayer)!
}

func blurPresentationLayer() -> FXBlurLayer {
    let blurLayer: FXBlurLayer? = self.blurLayer()
    return blurLayer?.presentation() ?? blurLayer
}

func setUpdate(_ updateInterval: TimeInterval) {
    self.updateInterval = updateInterval
    if self.updateInterval <= 0 {
        self.updateInterval = 1.0 / 60
    }
}

func setTintColor(_ tintColor: UIColor) {
    self.tintColor = tintColor
    setNeedsDisplay()
}

override func didMoveToSuperview() {
    super.didMoveToSuperview()
    layer.setNeedsDisplay()
}

override func didMoveToWindow() {
    super.didMoveToWindow()
    schedule()
}

func schedule() {
    if window && is`dynamic` && isBlurEnabled {
        FXBlurScheduler.sharedInstance().add(self)
    }
    else {
        FXBlurScheduler.sharedInstance().remove(self)
    }
}

override func setNeedsDisplay() {
    super.setNeedsDisplay()
    layer.setNeedsDisplay()
}

func shouldUpdate() -> Bool {
    let underlyingLayer: CALayer? = self.underlyingLayer()
    return underlyingLayer && !underlyingLayer?.isHidden && isBlurEnabled && FXBlurScheduler.sharedInstance().isBlurEnabled && !layer.presentation() ?? layer.bounds.isEmpty() && !underlyingLayer?.bounds.isEmpty()
}

func displayLayer(_ layer: CALayer) {
    updateAsynchronously(false, completion: nil)
}

override func action(forLayer layer: CALayer, forKey key: String) -> CAAction {
    if (key == "blurRadius") {
            //animations are enabled
        let action: CAAnimation? = (super.action(forLayer: layer, forKey: "backgroundColor") as? CAAnimation)
        if (action as? NSNull) != NSNull() {
            let animation = CABasicAnimation(keyPath: key)
            animation.fromValue = layer.presentation().value(forKey: key)
            //CAMediatiming attributes
            animation.beginTime = action?.beginTime
            animation.duration = action?.duration
            animation.speed = action?.speed
            animation.timeOffset = action?.timeOffset
            animation.repeatCount = action?.repeatCount
            animation.repeatDuration = action?.repeatDuration
            animation.autoreverses = action?.autoreverses
            animation.fillMode = action?.fillMode
            //CAAnimation attributes
            animation.timingFunction = action?.timingFunction
            animation.delegate = action?.delegate
            return animation
        }
    }
    return super.action(forLayer: layer, forKey: key)
}

func snapshotOfUnderlyingView() -> UIImage {
    let blurLayer: FXBlurLayer? = blurPresentationLayer()
    let underlyingLayer: CALayer? = self.underlyingLayer()
    let bounds: CGRect? = blurLayer?.convertRect(blurLayer?.bounds, to: underlyingLayer)
    lastUpdate = Date()
    var scale: CGFloat = 0.5
    if iterations != 0 {
        let blockSize: CGFloat = 12.0 / iterations
        scale = blockSize / max(blockSize * 2, blurLayer?.blurRadius())
        scale = 1.0 / floorf(1.0 / scale)
    }
    var size: CGSize? = bounds?.size
    if contentMode == .scaleToFill || contentMode == .scaleAspectFill || contentMode == .scaleAspectFit || contentMode == .redraw {
        //prevents edge artefacts
        size?.width = floorf(size?.width * scale) / scale
        size?.height = floorf(size?.height * scale) / scale
    }
    else if CFloat(UIDevice.current.systemVersion) < 7.0 && UIScreen.main.scale() == 1.0 {
        //prevents pixelation on old devices
        scale = 1.0
    }

    UIGraphicsBeginImageContextWithOptions(size, false, scale)
    let context: CGContext? = UIGraphicsGetCurrentContext()
    context.translateBy(x: -bounds?.origin?.x, y: -bounds?.origin?.y)
    let hiddenViews: [Any] = prepareUnderlyingViewForSnapshot()
    underlyingLayer?.render(in: context)
    restoreSuperview(afterSnapshot: hiddenViews)
    let snapshot: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return snapshot!
}

func prepareUnderlyingViewForSnapshot() -> [Any] {
    var blurlayer: CALayer? = blurLayer()
    let underlyingLayer: CALayer? = self.underlyingLayer()
    while blurlayer?.superlayer && blurlayer?.superlayer != underlyingLayer {
        blurlayer = blurlayer?.superlayer
    }
    var layers = [Any]()
    let index: Int? = (underlyingLayer?.sublayers? as NSArray).index(of: blurlayer)
    if index != NSNotFound {
        for i in index..<underlyingLayer?.sublayers?.count {
            let layer: CALayer? = underlyingLayer?.sublayers[i]
            if !layer?.isHidden {
                layer?.isHidden = true
                layers.append(layer)
            }
        }
    }
    return layers
}

func restoreSuperview(afterSnapshot hiddenLayers: [Any]) {
    for layer: CALayer in hiddenLayers {
        layer?.isHidden = false
    }
}

func blurredSnapshot(_ snapshot: UIImage, radius blurRadius: CGFloat) -> UIImage {
    return snapshot.blurredImage(withRadius: blurRadius, iterations: iterations, tintColor: tintColor)
}

func setLayerContents(_ image: UIImage) {
    layer?.contents = (image.cgImage as? Any)
    layer?.contentsScale = image.scale
}

func updateAsynchronously(_ async: Bool, completion: @escaping () -> Void) {
    if shouldUpdate() {
        let snapshot: UIImage? = snapshotOfUnderlyingView()
        if async {
            DispatchQueue.global(qos: .default).async(execute: {() -> Void in
                let blurredImage: UIImage? = self.blurredSnapshot(snapshot, radius: blurRadius())
                DispatchQueue.main.sync(execute: {() -> Void in
                    self.setLayerContents(blurredImage)
                    if completion {
                        completion()
                    }
                })
            })
        }
        else {
            setLayerContents(blurredSnapshot(snapshot, radius: blurPresentationLayer().blurRadius()))
            if completion {
                completion()
            }
        }
    }
    else if completion {
        completion()
    }

}