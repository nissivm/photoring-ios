//  Converted with Swiftify v1.0.6331 - https://objectivec2swift.com/
//
//  UIColor+HexToUIColor.swift
//  Not By Car Paris
//
//  Created by Nissi Vieira Miranda on 02/10/14.
//  Copyright (c) 2014 Nissi Vieira Miranda. All rights reserved.
//
import UIKit
extension UIColor {
    // takes @"#123456"

    convenience init(hex col: UInt32) {
        var r: UInt8
        var g: UInt8
        var b: UInt8
        b = col & 0xff
        g = (col >> 8) & 0xff
        r = (col >> 16) & 0xff
        return UIColor(red: CGFloat(Float(r) / 255.0), green: CGFloat(Float(g) / 255.0), blue: CGFloat(Float(b) / 255.0), alpha: CGFloat(1))
    }

    convenience init(hexString str: String) {
        let cStr = str.cString(using: String.Encoding.ascii)
        let x: Int = strtol(cStr + 1, nil, 16)
        return UIColor(hex: x)
    }

    // takes 0x123456
}